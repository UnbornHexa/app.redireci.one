/* ---- Requires ---- */

const DATA_HOSTS = require('../data/hosts')
const HELPER_HTTP = require('../helpers/http')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const URL = `${DATA_HOSTS().url.API_APP}/autenticacao`

  /* ---- Elements ---- */

  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  methods.registrar = (dados) => {
    const url = `${URL}/registrar`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().post(url, dados, null, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  methods.entrar = (dados) => {
    const url = `${URL}/entrar`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().post(url, dados, null, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  return methods
}

module.exports = Module

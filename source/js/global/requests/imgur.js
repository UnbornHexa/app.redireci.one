/* ---- Requires ---- */

const DATA_HOSTS = require('../data/hosts')
const HELPER_HTTP_FILE = require('../helpers/http-file')
const HELPER_TOKEN = require('../helpers/token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const URL = `${DATA_HOSTS().url.API_UPLOAD}/imgur`
  const TOKEN = HELPER_TOKEN().getToken()
  const HEADERS = HELPER_HTTP_FILE().setHeadersWithToken(TOKEN)

  /* ---- Elements ---- */

  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  methods.uploadIcone = (dados) => {
    const url = `${URL}/upload/icone`

    return new Promise((resolve, reject) => {
      HELPER_HTTP_FILE().postFile(url, dados, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  methods.uploadFundo = (dados) => {
    const url = `${URL}/upload/fundo`

    return new Promise((resolve, reject) => {
      HELPER_HTTP_FILE().postFile(url, dados, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  return methods
}

module.exports = Module

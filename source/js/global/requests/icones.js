/* ---- Requires ---- */

const DATA_HOSTS = require('../data/hosts')
const HELPER_HTTP = require('../helpers/http')
const HELPER_TOKEN = require('../helpers/token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const URL = `${DATA_HOSTS().url.API_APP}/icones`
  const TOKEN = HELPER_TOKEN().getToken()
  const HEADERS = HELPER_HTTP().setHeadersWithToken(TOKEN)

  /* ---- Elements ---- */

  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  methods.receberTodos = () => {
    const url = URL

    return new Promise((resolve, reject) => {
      HELPER_HTTP().get(url, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  return methods
}

module.exports = Module

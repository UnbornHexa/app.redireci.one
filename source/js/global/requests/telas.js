/* ---- Requires ---- */

const DATA_HOSTS = require('../data/hosts')
const HELPER_HTTP = require('../helpers/http')
const HELPER_TOKEN = require('../helpers/token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const URL = `${DATA_HOSTS().url.API_APP}/links`
  const TOKEN = HELPER_TOKEN().getToken()
  const USUARIO_ID = HELPER_TOKEN().readUsuarioId()
  const PAGINA_ID = HELPER_TOKEN().readPaginaId()
  const HEADERS = HELPER_HTTP().setHeadersWithToken(TOKEN)

  /* ---- Elements ---- */

  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  methods.receberTodos = () => {
    const url = `${URL}/${USUARIO_ID}/${PAGINA_ID}`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().get(url, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  methods.receberPorId = (idLink) => {
    const url = `${URL}/${USUARIO_ID}/${PAGINA_ID}/${idLink}`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().get(url, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  methods.adicionar = (dados) => {
    const url = `${URL}/${USUARIO_ID}/${PAGINA_ID}`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().post(url, dados, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 201) {
          appAlerta.alertar(body, 'sucesso')
          return resolve(body)
        }

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  methods.editar = (idLinks, dados) => {
    const url = `${URL}/${USUARIO_ID}/${PAGINA_ID}/${idLinks}`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().put(url, dados, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) {
          appAlerta.alertar(body, 'sucesso')
          return resolve(body)
        }

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  methods.deletar = (idLinks) => {
    const url = `${URL}/${USUARIO_ID}/${PAGINA_ID}/${idLinks}`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().delete(url, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) {
          appAlerta.alertar(body, 'sucesso')
          return resolve(body)
        }

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  methods.editarPosicionamento = (dados) => {
    const url = `${URL}/posicionamento/${USUARIO_ID}/${PAGINA_ID}`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().patch(url, dados, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) {
          appAlerta.alertar(body, 'sucesso')
          return resolve(body)
        }

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  return methods
}

module.exports = Module

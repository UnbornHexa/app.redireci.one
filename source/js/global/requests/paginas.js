/* ---- Requires ---- */

const DATA_HOSTS = require('../data/hosts')
const HELPER_HTTP = require('../helpers/http')
const HELPER_TOKEN = require('../helpers/token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const URL = `${DATA_HOSTS().url.API_APP}/paginas`
  const TOKEN = HELPER_TOKEN().getToken()
  const USUARIO_ID = HELPER_TOKEN().readUsuarioId()
  const PAGINA_ID = HELPER_TOKEN().readPaginaId()
  const HEADERS = HELPER_HTTP().setHeadersWithToken(TOKEN)

  /* ---- Elements ---- */

  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  methods.receberPorId = () => {
    const url = `${URL}/${USUARIO_ID}/${PAGINA_ID}`

    return new Promise((resolve, reject) => {
      HELPER_HTTP().get(url, HEADERS, response => {
        const { body } = response.body
        if (response?.status === 200) return resolve(body)

        appAlerta.alertar(body, 'erro')
        reject(new Error(body))
      })
    })
  }

  return methods
}

module.exports = Module

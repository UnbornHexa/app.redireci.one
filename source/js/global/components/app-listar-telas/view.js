/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_MOVER_ATIVO = 'ativo'

  /* ---- Methods ---- */

  methods.alternaSelecao = (divTela) => {
    const divMover = divTela.querySelector('div[name="mover"]')
    divMover.classList.toggle(CLASSE_MOVER_ATIVO)
  }

  methods.inverterPosicaoEntreTelas = (componente) => {
    const divTela1Proximo = componente.__state.moverTelas[0].nextSibling
    const divTela2Proximo = componente.__state.moverTelas[1].nextSibling

    componente.insertBefore(componente.__state.moverTelas[0], divTela2Proximo)
    componente.insertBefore(componente.__state.moverTelas[1], divTela1Proximo)
  }

  return methods
}

module.exports = Module

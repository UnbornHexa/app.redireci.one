/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../helpers/redirect')
const REQUEST_TELAS = require('../../requests/telas')
const VIEW = require('./view')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickTela = (componente) => {
    componente.addEventListener('click', callbackClickTela)
  }

  methods.clickMover = (componente) => {
    componente.addEventListener('click', (evento) => callbackClickMover(componente, evento))
  }

  /* ---- Callbacks ---- */

  function callbackClickTela (evento) {
    const elemento = 'div.tela button'
    if (!evento.target.matches(elemento)) return

    const divPai = evento.target.parentElement
    const inputId = divPai.querySelector('input[name="id"]')
    const idTela = inputId.value
    HELPER_REDIRECT().toPage(`telas/visualizar/${idTela}`)
  }

  function callbackClickMover (componente, evento) {
    const elemento = 'div[name="mover"]'
    if (!evento.target.matches(elemento)) return

    // seleciona elemento
    const divTela = evento.target.parentElement
    VIEW().alternaSelecao(divTela)
    componente.__state.moverTelas.push(divTela)

    if (componente.__state.moverTelas.length !== 2) return

    // desleciona elementos
    VIEW().alternaSelecao(componente.__state.moverTelas[0])
    VIEW().alternaSelecao(componente.__state.moverTelas[1])
    VIEW().inverterPosicaoEntreTelas(componente)
    componente.__state.moverTelas = []

    const dados = componente.exportarPosicionamento()
    REQUEST_TELAS().editarPosicionamento(dados)
  }

  return methods
}

module.exports = Module

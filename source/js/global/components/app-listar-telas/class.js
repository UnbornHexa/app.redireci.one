/* ---- Requires ---- */

const EVENTS = require('./events')

/* ---- Module ---- */

class AppListarTelas extends window.HTMLElement {
  constructor () {
    super()
    this.__state = { moverTelas: [] }

    EVENTS().clickTela(this)
    EVENTS().clickMover(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizarNovaTela (dados) {
    const div = document.createElement('div')
    div.classList.add('tela')

    div.innerHTML = `
    <input type="hidden" name="id" value="${dados?._id}">
    <div name="mover" class="mover">
      <img src="https://static.redireci.one/assets/global/seta-cima-baixo-cinza.svg">
    </div>
    <button type="button">
      <p>${dados?.nome}</p>
      <img src="https://static.redireci.one/assets/app/olho-cinza.svg">
    </button>
    `

    this.appendChild(div)
  }

  limparTelas () {
    this.innerHTML = ''
  }

  // Data

  importarTelas (telas) {
    this.limparTelas()

    for (const tela of telas) {
      this.renderizarNovaTela(tela)
    }
  }

  exportarPosicionamento () {
    const telas = this.querySelectorAll('.tela')
    const telasMap = Array.prototype.slice.apply(telas)

    const ordemTelas = telasMap.map((tela, indice) => {
      const idTela = tela.querySelector('input[name="id"]').value
      return { _id: idTela, posicao: indice }
    })

    return ordemTelas
  }
}

module.exports = AppListarTelas

/* ---- Requires ---- */

const HELPERS = require('./helpers')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickFormato = (componente) => {
    componente.addEventListener('click', (evento) => callbackClickFormato(componente, evento))
  }

  /* ---- Callbacks ---- */

  function callbackClickFormato (componente, evento) {
    const elemento = 'div'
    if (!evento.target.matches(elemento)) return

    const idFormato = evento.target.getAttribute('data-id')
    const formatoObj = HELPERS().pesquisarFormatoPorId(idFormato)
    componente.selecionar(evento.target, formatoObj)
  }

  return methods
}

module.exports = Module

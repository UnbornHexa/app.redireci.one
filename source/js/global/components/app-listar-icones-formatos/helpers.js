/* ---- Requires ---- */

const DATA = require('./data')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.pesquisarFormatoPorId = (idFormato) => {
    const filtrarPorId = formato => (formato.id === Number(idFormato))
    return DATA.filter(filtrarPorId)[0]
  }

  methods.pesquisarFormatoPorNome = (nomeFormato) => {
    const filtrarPorFormato = formato => (formato.formato === nomeFormato)
    return DATA.filter(filtrarPorFormato)[0]
  }

  return methods
}

module.exports = Module

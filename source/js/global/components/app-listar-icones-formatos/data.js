/* ---- Module ---- */

const Module = [
  { id: 1, formato: 'quadrado' },
  { id: 2, formato: 'arredondado' },
  { id: 3, formato: 'circulo' }
]

module.exports = Module

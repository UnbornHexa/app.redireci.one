/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_FORMATO_SELECAO = 'selecionado'

  /* ---- Methods ---- */

  methods.selecionarFormato = (componente, elemento) => {
    methods.deslecionarFormatos(componente)
    elemento.classList.add(CLASSE_FORMATO_SELECAO)
  }

  methods.deslecionarFormatos = (componente) => {
    const formatosSelecionados = componente.querySelectorAll(`.${CLASSE_FORMATO_SELECAO}`)
    if (formatosSelecionados.length === 0) return

    formatosSelecionados[0].classList.remove(CLASSE_FORMATO_SELECAO)
  }

  methods.definirImagemFundo = (componente, imagem) => {
    const divsFormato = componente.querySelectorAll('div')
    for (const div of divsFormato) {
      div.style.backgroundImage = `url(${imagem})`
    }
  }

  return methods
}

module.exports = Module

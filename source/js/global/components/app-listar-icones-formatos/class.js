/* ---- Requires ---- */

const DATA = require('./data')
const EVENTS = require('./events')
const VIEW = require('./view')

/* ---- Module ---- */

class AppListarIconesFormatos extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      formatoSelecionado: {
        id: '',
        formato: ''
      }
    }

    this.renderizar()
    EVENTS().clickFormato(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizar () {
    for (const formato of DATA) {
      this.renderizarNovoFormato(formato)
    }
  }

  renderizarNovoFormato (dados) {
    const formato = document.createElement('div')
    formato.classList.add(dados?.formato)
    formato.setAttribute('data-id', dados?.id)
    this.appendChild(formato)
  }

  // Util

  selecionar (elemento, formatoObj) {
    if (!elemento || !formatoObj) return

    VIEW().selecionarFormato(this, elemento)

    this.__state.formatoSelecionado.id = formatoObj.id
    this.__state.formatoSelecionado.formato = formatoObj.formato
  }

  vazio () {
    return (!this.__state.formatoSelecionado.formato)
  }

  definirImagemFundo (imagem) {
    VIEW().definirImagemFundo(this, imagem)
  }

  limpar () {
    this.__state.formatoSelecionado.id = ''
    this.__state.formatoSelecionado.formato = ''
    this.definirImagemFundo('')
    VIEW().deslecionarFormatos(this)
  }

  // Data

  exportarFormatoSelecionado () {
    return this.__state.formatoSelecionado.formato
  }
}

module.exports = AppListarIconesFormatos

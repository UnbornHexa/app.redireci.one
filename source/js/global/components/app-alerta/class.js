/* ---- Requires ---- */

const VIEW = require('./view')
const EVENTS = require('./events')

/* ---- Module ---- */

class AppAlerta extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      cancelarDesaparecimento: null
    }

    this.renderizar()
    EVENTS().clickEsconder(this)
  }

  /* ---- Methods ---- */

  renderizar () {
    this.innerHTML = `
    <div class="texto">
      <h3 name="titulo">TÍTUTLO</h3>
      <p name="descricao">Descrição</p>
    </div>
    `
  }

  // Controls

  alertar (mensagem, tipo = 'aviso') {
    VIEW().definirTitulo(this, tipo)
    VIEW().definirMensagem(this, mensagem)

    VIEW().mostrar(this)
    VIEW().definirEstilo(this, tipo)

    this.esconderAutomaticamente()
  }

  /* ---- Aux Functions ---- */

  esconderAutomaticamente () {
    clearTimeout(this.__state.cancelarDesaparecimento)
    this.__state.cancelarDesaparecimento =
      setTimeout(() => VIEW().esconder(this), 5000)
  }
}

module.exports = AppAlerta

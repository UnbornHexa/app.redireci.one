/* ---- Requires ---- */

const VIEW = require('./view')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickEsconder = (componente) => {
    componente.addEventListener('click', () => callbackClickEsconder(componente))
  }

  /* ---- Callbacks ---- */

  function callbackClickEsconder (componente) {
    VIEW().esconder(componente)
  }

  return methods
}

module.exports = Module

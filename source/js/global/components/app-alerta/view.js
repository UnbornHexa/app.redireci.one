/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_MOSTRAR = 'mostrar'
  const CLASSE_OCULTAR = 'ocultar'

  /* ---- Methods ---- */

  // Visibilidade

  methods.mostrar = (componente) => {
    componente.classList.add(CLASSE_MOSTRAR)
    componente.classList.remove(CLASSE_OCULTAR)
  }

  methods.esconder = (componente) => {
    componente.classList.add(CLASSE_OCULTAR)
    componente.classList.remove(CLASSE_MOSTRAR)
  }

  // Estilo

  methods.definirEstilo = (componente, classe) => {
    methods.removerEstilo(componente)
    componente.classList.add(classe)
  }

  methods.removerEstilo = (componente) => {
    componente.classList.remove('aviso')
    componente.classList.remove('sucesso')
    componente.classList.remove('erro')
  }

  // Texto

  methods.definirMensagem = (componente, mensagem) => {
    const pMensagem = componente.querySelector('p[name="descricao"]')
    pMensagem.innerText = mensagem
  }

  methods.definirTitulo = (componente, titulo) => {
    const pTitulo = componente.querySelector('h3[name="titulo"]')
    pTitulo.innerText = titulo.toUpperCase()
  }

  return methods
}

module.exports = Module

/* ---- Module ---- */

const Module = [
  // regular
  { id: 1, fonte: 'cyntho_next_regular' },
  { id: 2, fonte: 'gilroy_bold' },
  { id: 3, fonte: 'fredoka_one_regular' },
  { id: 4, fonte: 'audio_wide_regular' },
  { id: 5, fonte: 'yeseva_one_regular' },
  { id: 6, fonte: 'chewy_regular' },
  { id: 7, fonte: 'permanet_maker_regular' },

  // bold
  { id: 8, fonte: 'bangers_regular' },
  { id: 9, fonte: 'piedra_regular' },
  { id: 10, fonte: 'alfa_slab_one_regular' },

  // light
  { id: 11, fonte: 'orbitron' },
  { id: 12, fonte: 'montserrat_light' },
  { id: 13, fonte: 'gloria_hallelujah_regular' },
  { id: 14, fonte: 'sansitas_washed' },
  { id: 15, fonte: 'shadows_in_to_light' },

  // piche
  { id: 16, fonte: 'rock_salt_regular' }
]

module.exports = Module

/* ---- Requires ---- */

const HELPERS = require('./helpers')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickFonte = (componente) => {
    componente.addEventListener('click', (evento) => callbackClickFonte(componente, evento))
  }

  /* ---- Callbacks ---- */

  function callbackClickFonte (componente, evento) {
    const elemento = '.fonte'
    if (!evento.target.matches(elemento)) return

    const pFonte = evento.target.querySelector('p')
    const idFonte = pFonte.getAttribute('data-id')

    const fonteObj = HELPERS().pesquisarFontePorId(idFonte)
    componente.selecionar(evento.target, fonteObj)
  }

  return methods
}

module.exports = Module

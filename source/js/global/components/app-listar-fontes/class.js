/* ---- Requires ---- */

const DATA = require('./data')
const EVENTS = require('./events')
const HELPERS = require('./helpers')
const VIEW = require('./view')

/* ---- Module ---- */

class AppListarFontes extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      selecionado: {
        id: '',
        fonte: ''
      }
    }

    this.renderizar()
    EVENTS().clickFonte(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizar () {
    const fontes = DATA
    for (const fonte of fontes) {
      this.renderizarNovaFonte(fonte)
    }
  }

  renderizarNovaFonte (dados) {
    const classeFonte = `fonte_${dados?.fonte}`

    const fonte = document.createElement('div')
    fonte.classList.add('fonte')
    fonte.innerHTML = `<p class="${classeFonte}" data-id="${dados?.id}">Abc123</p>`
    this.appendChild(fonte)
  }

  // Util

  selecionar (elemento, fonteObj) {
    if (!elemento || !fonteObj) return

    VIEW().adicionarClasseSelecao(this, elemento)

    this.__state.selecionado.id = fonteObj.id
    this.__state.selecionado.fonte = fonteObj.fonte
  }

  vazio () {
    return (!this.__state.selecionado.fonte)
  }

  // Data

  importarFonteSelecionada (nome) {
    const fonteObj = HELPERS().pesquisarFontePorNome(nome)
    if (!fonteObj) return

    const pFonte = VIEW().obterPFontePorId(this, fonteObj.id)
    const divPai = pFonte.parentElement

    this.selecionar(divPai, fonteObj)
  }

  exportarFonteSelecionada () {
    return this.__state.selecionado.fonte
  }
}

module.exports = AppListarFontes

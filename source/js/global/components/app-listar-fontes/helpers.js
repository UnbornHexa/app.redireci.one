/* ---- Requires ---- */

const DATA = require('./data')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.pesquisarFontePorId = (idFonte) => {
    const filtrarPorId = fonte => (fonte.id === Number(idFonte))
    return DATA.filter(filtrarPorId)[0]
  }

  methods.pesquisarFontePorNome = (nomeFonte) => {
    const filtrarPorNome = fonte => (fonte.fonte === nomeFonte)
    return DATA.filter(filtrarPorNome)[0]
  }

  return methods
}

module.exports = Module

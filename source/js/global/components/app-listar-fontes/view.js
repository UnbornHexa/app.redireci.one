/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SELECAO = 'selecionado'

  /* ---- Methods ---- */

  // Selecionado

  methods.adicionarClasseSelecao = (componente, elemento) => {
    methods.removerSelecoes(componente)
    elemento.classList.add(CLASSE_SELECAO)
  }

  methods.removerSelecoes = (componente) => {
    const fontesSelecionadas = componente.querySelectorAll(`.fonte.${CLASSE_SELECAO}`)
    if (fontesSelecionadas.length === 0) return

    fontesSelecionadas[0].classList.remove(CLASSE_SELECAO)
  }

  // Pesquisa

  methods.obterPFontePorId = (componente, idFonte) => {
    return componente.querySelector(`.fonte p[data-id="${idFonte}"]`)
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const EVENTS = require('./events')
const HELPERS = require('./helpers')
const VIEW = require('./view')

/* ---- Module ---- */

class AppListarFundos extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      galeria: {
        fundos: [],
        fundosFiltrados: []
      },
      fundoSelecionado: { url: '' },
      debouncePesquisa: null
    }

    this.renderizar()
    EVENTS().clickFundo(this)
    EVENTS().keyBusca(this)
    EVENTS().scrollGaleria(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizar () {
    this.innerHTML = `
    <input name="busca" type="text" spellcheck="false" placeholder="Procurar fundo">
    <div name="galeria" class="galeria"></div>
    `
  }

  renderizarGaleria () {
    const fundos = this.__state.galeria.fundosFiltrados
    for (const fundo of fundos) {
      this.renderizarNovoFundo(fundo)
    }
    VIEW().acionarGaleriaScroll(this)
  }

  renderizarNovoFundo (fundo) {
    const divItem = document.createElement('div')
    divItem.classList.add('item')

    const imgUrl = document.createElement('img')
    imgUrl.setAttribute('name', 'url')
    imgUrl.setAttribute('data-src', fundo?.url)

    const inputId = document.createElement('input')
    inputId.setAttribute('name', 'id')
    inputId.setAttribute('type', 'hidden')
    inputId.value = fundo?._id

    divItem.appendChild(imgUrl)
    divItem.appendChild(inputId)

    const divGaleria = this.querySelector('div[name="galeria"]')
    divGaleria.appendChild(divItem)

    if (fundo?.url !== this.__state.fundoSelecionado.url) return
    VIEW().selecionarFundo(this, divItem)
  }

  // Util

  selecionar (elemento, fundoObj) {
    if (!elemento || !fundoObj) return

    VIEW().selecionarFundo(this, elemento)
    this.__state.fundoSelecionado.id = fundoObj._id
    this.__state.fundoSelecionado.url = fundoObj.url
  }

  vazio () {
    return (!this.__state.fundoSelecionado.url)
  }

  // Data

  importarFundos (fundos) {
    VIEW().limparGaleria(this)
    this.__state.galeria.fundos = fundos
    this.__state.galeria.fundosFiltrados = fundos
    this.renderizarGaleria()
  }

  importarFundoSelecionado (url) {
    this.__state.fundoSelecionado.url = url
  }

  exportarFundoSelecionado () {
    return this.__state.fundoSelecionado.url
  }

  // Filter

  limparFiltro () {
    this.__state.galeria.fundosFiltrados = this.__state.galeria.fundos
    VIEW().limparGaleria(this)
    this.renderizarGaleria()
  }

  filtrar (texto) {
    const fundosFiltrados = []
    const textoPesquisado = HELPERS().removerCaracteresEspeciais(texto)

    for (const fundo of this.__state.galeria.fundos) {
      for (const palavraChave of fundo?.palavrasChaves) {
        const palavra = HELPERS().removerCaracteresEspeciais(palavraChave)
        if (palavra.includes(textoPesquisado)) {
          fundosFiltrados.push(fundo)
          break
        }
      }
    }

    this.__state.galeria.fundosFiltrados = fundosFiltrados
    VIEW().limparGaleria(this)
    this.renderizarGaleria()
  }
}

module.exports = AppListarFundos

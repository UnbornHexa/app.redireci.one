/* ---- Requires ---- */

const HELPERS = require('./helpers')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickFundo = (componente) => {
    componente.addEventListener('click', (evento) => callbackClickFundo(componente, evento))
  }

  methods.keyBusca = (componente) => {
    const inputBusca = componente.querySelector('input[name="busca"]')
    inputBusca.addEventListener('keyup', (evento) => callbackKeyBusca(componente, evento))
  }

  methods.scrollGaleria = (componente) => {
    const divGaleria = componente.querySelector('div[name="galeria"]')
    divGaleria.addEventListener('scroll', (evento) => callbackScrollGaleria(componente, evento))
  }

  /* ---- Callbacks ---- */

  function callbackClickFundo (componente, evento) {
    const elemento = '.item'
    if (!evento.target.matches(elemento)) return

    const inputId = evento.target.querySelector('input')
    const idFundo = inputId.value

    const fundoObj = HELPERS().pesquisarFundoPorId(componente, idFundo)
    componente.selecionar(evento.target, fundoObj)
  }

  function callbackKeyBusca (componente, evento) {
    clearTimeout(componente.__state.debouncePesquisa)
    componente.__state.debouncePesquisa = setTimeout(pesquisar(componente, evento), 500)

    function pesquisar (componente, evento) {
      const termoPesquisado = evento.target.value
      if (termoPesquisado.length <= 3) {
        componente.limparFiltro()
        return
      }
      componente.filtrar(termoPesquisado)
    }
  }

  function callbackScrollGaleria (componente, evento) {
    const divGaleria = componente.querySelector('div[name="galeria"]')
    const alturaPagina = evento.target.scrollTop + divGaleria.offsetHeight + 500
    const imagens = componente.querySelectorAll('img[data-src]')

    for (const imagem of imagens) {
      if (alturaPagina <= imagem.offsetTop) return
      imagem.src = imagem.getAttribute('data-src')
    }
  }

  return methods
}

module.exports = Module

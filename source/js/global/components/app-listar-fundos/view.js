/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SELECAO = 'selecionado'

  /* ---- Methods ---- */

  methods.limparGaleria = (componente) => {
    const divGaleria = componente.querySelector('div[name="galeria"]')
    divGaleria.innerHTML = ''
  }

  methods.acionarGaleriaScroll = (componente) => {
    const divGaleria = componente.querySelector('div[name="galeria"]')
    const evento = new window.CustomEvent('scroll')
    divGaleria.dispatchEvent(evento)
  }

  // Classe Selecao

  methods.selecionarFundo = (componente, elemento) => {
    methods.deselecionarFundos(componente)
    elemento.classList.add(CLASSE_SELECAO)
  }

  methods.deselecionarFundos = (componente) => {
    const fundoSelecionado = componente.querySelectorAll(`.item.${CLASSE_SELECAO}`)
    if (fundoSelecionado.length === 0) return

    fundoSelecionado[0].classList.remove(CLASSE_SELECAO)
  }

  return methods
}

module.exports = Module

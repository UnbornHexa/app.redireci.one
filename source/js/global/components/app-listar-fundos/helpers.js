/* ---- Requires ---- */

const HELPER_ACCENT = require('../../helpers/accent')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  // Pesquisa

  methods.pesquisarFundoPorId = (componente, idFundo) => {
    const filtrarPorId = fundo => (fundo._id === idFundo)
    return componente.__state.galeria.fundos.filter(filtrarPorId)[0]
  }

  methods.pesquisarFundoPorUrl = (componente, urlFundo) => {
    const filtrarPorUrl = fundo => (fundo.url === urlFundo)
    return componente.__state.galeria.fundos.filter(filtrarPorUrl)[0]
  }

  // Filtrar

  methods.removerCaracteresEspeciais = (texto) => {
    const textoSanitizado = texto?.toString()?.toUpperCase()
    return HELPER_ACCENT().remove(textoSanitizado)
  }

  return methods
}

module.exports = Module

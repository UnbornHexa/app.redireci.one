/* ---- requires ---- */

const EVENTS = require('./events')
const HELPERS = require('./helpers')
const VIEW = require('./view')

/* ---- Module ---- */

class AppSelecionarCor extends window.HTMLElement {
  constructor () {
    super()
    this.__state = { cor: '' }

    this.renderizar()
    EVENTS().clickAmostraCor(this)
    EVENTS().inputKeyBloquearCaracteres(this)
    EVENTS().inputKeySelecionarCor(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizar () {
    this.innerHTML = `
    <input type="text" spellcheck="false" name="cor" placeholder="#ffffff" maxlength="7">
    <div name="preview" class="visualizar"></div>
    <div name="amostra_cores" class="amostra_cores">
      <div name="cor" data-cor="#ffffff" class="cor cor1"></div>
      <div name="cor" data-cor="#000000" class="cor cor2"></div>
      <div name="cor" data-cor="#c7c7c7" class="cor cor3"></div>
      <div name="cor" data-cor="#a0a0a0" class="cor cor4"></div>
      <div name="cor" data-cor="#6c6c6c" class="cor cor5"></div>
      <div name="cor" data-cor="#d62828" class="cor cor6"></div>
      <div name="cor" data-cor="#f77f00" class="cor cor7"></div>
      <div name="cor" data-cor="#52b788" class="cor cor8"></div>
      <div name="cor" data-cor="#4ea8de" class="cor cor9"></div>
      <div name="cor" data-cor="#d4d700" class="cor cor10"></div>
      <div name="cor" data-cor="#5a189a" class="cor cor11"></div>
      <div name="cor" data-cor="#a5ffd6" class="cor cor12"></div>
      <div name="cor" data-cor="#ffb700" class="cor cor13"></div>
      <div name="cor" data-cor="#00bbf9" class="cor cor14"></div>
      <div name="cor" data-cor="#3a0ca3" class="cor cor15"></div>
      <div name="cor" data-cor="#33415c" class="cor cor16"></div>
      <div name="cor" data-cor="#f38375" class="cor cor17"></div>
      <div name="cor" data-cor="#724cf9" class="cor cor18"></div>
      <div name="cor" data-cor="#775144" class="cor cor19"></div>
      <div name="cor" data-cor="#ccff33" class="cor cor20"></div>
      <div name="cor" data-cor="#ff6000" class="cor cor21"></div>
      <div name="cor" data-cor="#ee4266" class="cor cor22"></div>
      <div name="cor" data-cor="#5c4d3c" class="cor cor23"></div>
      <div name="cor" data-cor="#ff0000" class="cor cor24"></div>
      <div name="cor" data-cor="#aa4465" class="cor cor25"></div>
      <div name="cor" data-cor="#474056" class="cor cor26"></div>
      <div name="cor" data-cor="#1c0f13" class="cor cor27"></div>
      <div name="cor" data-cor="#94ecbe" class="cor cor28"></div>
      <div name="cor" data-cor="#dc0073" class="cor cor29"></div>
      <div name="cor" data-cor="#edc531" class="cor cor30"></div>
      <div name="cor" data-cor="#3bceac" class="cor cor31"></div>
      <div name="cor" data-cor="#ffc857" class="cor cor32"></div>
      <div name="cor" data-cor="#c9a227" class="cor cor33"></div>
      <div name="cor" data-cor="#a663cc" class="cor cor34"></div>
      <div name="cor" data-cor="#400000" class="cor cor35"></div>
      <div name="cor" data-cor="#89fc00" class="cor cor36"></div>
    </div>
    `
  }

  // Util

  vazio () {
    return (!this.__state.cor)
  }

  selecionar (hex) {
    const cor = HELPERS().sanitizarCor(hex)
    if (cor.length !== 6) return

    this.__state.cor = cor
    VIEW().definirCorInput(this, cor)
    VIEW().definirCorPreview(this, cor)
    VIEW().selecionarCorAmostra(this, cor)
  }

  // Data

  importarCor (hex) {
    if (!hex) return

    const cor = HELPERS().sanitizarCor(hex)
    if (cor.length !== 6) return

    this.selecionar(cor)
  }

  exportarCor () {
    return `#${this.__state.cor}`
  }
}

module.exports = AppSelecionarCor

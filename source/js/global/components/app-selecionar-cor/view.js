/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const COR_PLACEHOLDER = '555560'
  const CLASSE_AMOSTRA_SELECIONADA = 'selecionado'

  /* ---- Methods ---- */

  // Input

  methods.definirCorInput = (componente, hex) => {
    const input = componente.querySelector('input[name="cor"]')
    input.value = hex
  }

  // Preview

  methods.limparCorPreview = (componente) => {
    methods.definirCorPreview(componente, COR_PLACEHOLDER)
  }

  methods.definirCorPreview = (componente, hex) => {
    const divPreview = componente.querySelector('div[name="preview"]')
    divPreview.style.backgroundColor = `#${hex}`
  }

  // Amostras

  methods.selecionarCorAmostra = (componente, hex) => {
    methods.removerSelecaoAmostras(componente)

    const divAmostras = componente.querySelector('div[name="amostra_cores"]')
    const divCor = divAmostras.querySelector(`div[data-cor="#${hex}"]`)

    if (!divCor) return
    divCor.classList.add(CLASSE_AMOSTRA_SELECIONADA)
  }

  methods.removerSelecaoAmostras = (componente) => {
    const divAmostras = componente.querySelector('div[name="amostra_cores"]')
    const amostras = divAmostras.querySelectorAll(`div[name="cor"].${CLASSE_AMOSTRA_SELECIONADA}`)
    if (amostras.length === 0) return

    amostras[0].classList.remove(CLASSE_AMOSTRA_SELECIONADA)
  }

  return methods
}

module.exports = Module

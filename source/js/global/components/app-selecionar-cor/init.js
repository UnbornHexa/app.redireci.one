/* ---- Requires ---- */

const CLASS = require('./class')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.iniciar = () => {
    window.customElements.define('app-selecionar-cor', CLASS)
  }

  return methods
}

module.exports = Module

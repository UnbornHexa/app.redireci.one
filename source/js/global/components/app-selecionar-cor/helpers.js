/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.sanitizarCor = (hex) => {
    const corSemHashtag = hex.replace(/#/, '')
    const corLetraMinuscula = corSemHashtag.toString().toLowerCase()
    return corLetraMinuscula
  }

  return methods
}

module.exports = Module

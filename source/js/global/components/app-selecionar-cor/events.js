/* ---- Requires ---- */

const HELPERS = require('./helpers')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickAmostraCor = (componente) => {
    const divAmostra = componente.querySelector('div[name="amostra_cores"]')
    divAmostra.addEventListener('click', (evento) => callbackClickAmostraCor(componente, evento))
  }

  methods.inputKeyBloquearCaracteres = (componente) => {
    const input = componente.querySelector('input[name="cor"]')
    input.addEventListener('keyup', (evento) => callbackBloquearCaracteres(evento))
    input.addEventListener('keypress', (evento) => callbackBloquearCaracteres(evento))
  }

  methods.inputKeySelecionarCor = (componente) => {
    const input = componente.querySelector('input[name="cor"]')
    input.addEventListener('keyup', (evento) => callbackSelecionarCor(componente, evento))
    input.addEventListener('keypress', (evento) => callbackSelecionarCor(componente, evento))
  }

  /* ---- Callbacks ---- */

  function callbackClickAmostraCor (componente, evento) {
    const elemento = 'div[name="cor"]'
    if (!evento.target.matches(elemento)) return

    const cor = evento.target.getAttribute('data-cor')
    componente.selecionar(cor)
  }

  function callbackBloquearCaracteres (evento) {
    const texto = evento.target.value
    const regex = /[^0-9abcdef]/gi
    const cor = texto.replace(regex, '')
    evento.target.value = cor
  }

  function callbackSelecionarCor (componente, evento) {
    const hex = evento.target.value
    const cor = HELPERS().sanitizarCor(hex)
    componente.selecionar(cor)
  }

  return methods
}

module.exports = Module

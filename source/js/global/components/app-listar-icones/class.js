/* ---- Requires ---- */

const EVENTS = require('./events')
const HELPERS = require('./helpers')
const VIEW = require('./view')

/* ---- Module ---- */

class AppListarIcones extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      galeria: {
        icones: [],
        iconesFiltrados: []
      },
      iconeSelecionado: { url: '' },
      debouncePesquisa: null
    }

    this.renderizar()
    EVENTS().clickIcone(this)
    EVENTS().inputBusca(this)
    EVENTS().scrollGaleria(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizar () {
    this.innerHTML = `
    <input name="busca" spellcheck="false" type="text" placeholder="Pesquisar por ícone...">
    <div name="galeria" class="galeria"></div>
    `
  }

  renderizarNovoIcone (icone) {
    const divItem = document.createElement('div')
    divItem.classList.add('item')

    const imgUrl = document.createElement('img')
    imgUrl.setAttribute('name', 'url')
    imgUrl.setAttribute('data-src', icone.url)

    const inputId = document.createElement('input')
    inputId.setAttribute('name', 'id')
    inputId.setAttribute('type', 'hidden')
    inputId.value = icone._id

    divItem.appendChild(imgUrl)
    divItem.appendChild(inputId)

    const divGaleria = this.querySelector('div[name="galeria"]')
    divGaleria.appendChild(divItem)

    if (icone.url !== this.__state.iconeSelecionado.url) return
    VIEW().selecionarIcone(this, divItem)
  }

  /* ----- */

  carregarIcones () {
    const icones = this.__state.galeria.iconesFiltrados
    for (const icone of icones) {
      this.renderizarNovoIcone(icone)
    }
    EVENTS().emitirEventoScrollGaleria(this)
  }

  // Util

  selecionar (elemento, iconeObj) {
    if (!elemento || !iconeObj) return

    VIEW().selecionarIcone(this, elemento)
    this.__state.iconeSelecionado.id = iconeObj._id
    this.__state.iconeSelecionado.url = iconeObj.url
  }

  vazio () {
    return (!this.__state.iconeSelecionado.url)
  }

  // Data

  importarIcones (icones) {
    VIEW().limparGaleria(this)

    this.__state.galeria.icones = icones
    this.__state.galeria.iconesFiltrados = icones
    this.carregarIcones()
  }

  importarIconeSelecionado (url) {
    this.__state.iconeSelecionado.url = url
  }

  exportarIconeSelecionado () {
    return this.__state.iconeSelecionado.url
  }

  // Filter

  limparFiltro () {
    this.__state.galeria.iconesFiltrados = this.__state.galeria.icones
    VIEW().limparGaleria(this)
    this.carregarIcones()
  }

  filtrar (termo) {
    const iconesFiltrados = []
    const textoPesquisado = HELPERS().removerCaracteresEspeciais(termo)

    for (const icone of this.__state.galeria.icones) {
      for (const palavraChave of icone?.palavrasChaves) {
        const palavra = HELPERS().removerCaracteresEspeciais(palavraChave)
        if (palavra.includes(textoPesquisado)) {
          iconesFiltrados.push(icone)
          break
        }
      }
    }

    this.__state.galeria.iconesFiltrados = iconesFiltrados
    VIEW().limparGaleria(this)
    this.carregarIcones()
  }
}

module.exports = AppListarIcones

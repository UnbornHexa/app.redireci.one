/* ---- Requires ---- */

const HELPERS = require('./helpers')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickIcone = (componente) => {
    componente.addEventListener('click', (evento) => callbackClickIcone(componente, evento))
  }

  methods.inputBusca = (componente) => {
    componente.addEventListener('keyup', (evento) => callbackInputBusca(componente, evento))
  }

  methods.scrollGaleria = (componente) => {
    const divGaleria = componente.querySelector('div[name="galeria"]')
    divGaleria.addEventListener('scroll', (evento) => callbackScrollGaleria(componente, evento))
  }

  /* ---- Callbacks ---- */

  function callbackClickIcone (componente, evento) {
    const elemento = '.item'
    if (!evento.target.matches(elemento)) return

    const inputId = evento.target.querySelector('input')
    const idIcone = inputId.value

    const iconeObj = HELPERS().pesquisarIconePorId(componente, idIcone)
    componente.selecionar(evento.target, iconeObj)
  }

  function callbackInputBusca (componente, evento) {
    const elemento = 'input[name="busca"]'
    if (!evento.target.matches(elemento)) return

    clearTimeout(componente.__state.debouncePesquisa)
    componente.__state.debouncePesquisa = setTimeout(() => pesquisar(), 500)

    function pesquisar () {
      const termoPesquisado = evento.target.value
      if (termoPesquisado.length <= 3) {
        componente.limparFiltro()
        return
      }
      componente.filtrar(termoPesquisado)
    }
  }

  function callbackScrollGaleria (componente, evento) {
    const divGaleria = componente.querySelector('div[name="galeria"]')

    const alturaPagina = evento.target.scrollTop + divGaleria.offsetHeight + 500
    const imagens = componente.querySelectorAll('img[data-src]')

    for (const imagem of imagens) {
      if (alturaPagina <= imagem.offsetTop) return

      const src = imagem.getAttribute('data-src')
      imagem.src = src
    }
  }

  /* ---- Aux Functions ---- */

  methods.emitirEventoScrollGaleria = (componente) => {
    const divGaleria = componente.querySelector('div[name="galeria"]')
    const evento = new window.CustomEvent('scroll')
    divGaleria.dispatchEvent(evento)
  }

  return methods
}

module.exports = Module

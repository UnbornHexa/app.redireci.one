/* ---- Requires ---- */

const HELPER_ACCENT = require('../../helpers/accent')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.pesquisarIconePorId = (componente, idIcone) => {
    const filtrarPorId = icone => (icone._id === idIcone)
    return componente.__state.galeria.icones.filter(filtrarPorId)[0]
  }

  methods.pesquisarIconePorUrl = (componente, urlIcone) => {
    const filtrarPorUrl = icone => (icone.url === urlIcone)
    return componente.__state.galeria.icones.filter(filtrarPorUrl)[0]
  }

  // Filtrar

  methods.removerCaracteresEspeciais = (texto) => {
    const textoSanitizado = texto?.toString()?.toUpperCase()
    return HELPER_ACCENT().remove(textoSanitizado)
  }

  return methods
}

module.exports = Module

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_ICONE_SELECIONADO = 'selecionado'

  /* ---- Methods ---- */

  // Galeria

  methods.limparGaleria = (componente) => {
    const divGaleria = componente.querySelector('div[name="galeria"]')
    divGaleria.innerHTML = ''
  }

  // Selecao

  methods.selecionarIcone = (componente, elemento) => {
    methods.desativarSelecoes(componente)
    elemento.classList.add(CLASSE_ICONE_SELECIONADO)
  }

  methods.desativarSelecoes = (componente) => {
    const iconesSelecionado = componente.querySelectorAll(`.item.${CLASSE_ICONE_SELECIONADO}`)
    if (iconesSelecionado.length === 0) return

    iconesSelecionado[0].classList.remove(CLASSE_ICONE_SELECIONADO)
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const EVENTS = require('./events')

/* ----  Module ---- */

class AppEnviarImagem extends window.HTMLElement {
  constructor () {
    super()
    this.__state = { formData: null }

    this.renderizar()
    EVENTS().click(this)
    EVENTS().changeFile(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizar () {
    const imgPlaceholder = 'https://static.redireci.one/assets/app/camera-cinza.svg'
    this.innerHTML = `
    <img name="preview" src="${imgPlaceholder}">
    <p class="placeholder">Clique para Enviar</p>
    <input type="file" name="file" hidden="true" accept="image/jpeg, image/png">
    `
  }

  // Utils

  vazio () {
    return (!this.__state.formData)
  }

  limpar () {
    this.__state.formData = null
  }

  // Data

  exportarFormData () {
    return this.__state.formData
  }

  exportarImagem () {
    const inputFile = this.querySelector('input[type="file"]')
    const imagem = inputFile.files[0]

    const fileReader = new window.FileReader()
    fileReader.readAsDataURL(imagem)

    return new Promise(resolve => {
      fileReader.onload = () => {
        return resolve(fileReader.result)
      }
    })
  }
}

module.exports = AppEnviarImagem

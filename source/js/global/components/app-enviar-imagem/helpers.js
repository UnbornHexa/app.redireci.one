/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const MB20 = 20971520

  /* ---- Elements ---- */

  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  methods.verificarTamanhoMaximo = (imagem) => {
    if (imagem.size > MB20) appAlerta.alertar('A imagem tem um tamanho muito grande.')
    else return true

    return false
  }

  methods.verificarExtensao = (imagem) => {
    const allowedTypes = [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]

    if (!allowedTypes.includes(imagem.type)) appAlerta.alertar('A imagem tem um formato inválido.')
    else return true

    return false
  }

  methods.converterFormData = (imagem) => {
    const formData = new window.FormData()
    formData.append('arquivo', imagem)
    return formData
  }

  return methods
}

module.exports = Module

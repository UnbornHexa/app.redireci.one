/* ---- Requires ---- */

const HELPERS = require('./helpers')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.click = (componente) => {
    componente.addEventListener('click', () => callbackClick(componente))
  }

  methods.changeFile = (componente) => {
    const inputFile = componente.querySelector('input[name="file"]')
    inputFile.addEventListener('change', () => callbackChangeFile(componente, inputFile))
  }

  /* ---- Callbacks ---- */

  function callbackClick (componente) {
    const inputFile = componente.querySelector('input[name="file"]')
    inputFile.click()
  }

  function callbackChangeFile (componente, inputFile) {
    const imagem = inputFile.files[0]

    if (!HELPERS().verificarTamanhoMaximo(imagem)) return
    if (!HELPERS().verificarExtensao(imagem)) return

    const formData = HELPERS().converterFormData(imagem)
    componente.__state.formData = formData
  }

  return methods
}

module.exports = Module

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SELECAO = 'selecionado'

  /* ---- Methods ---- */

  // Selecionado

  methods.adicionarClasseSelecao = (componente, elemento) => {
    methods.removerSelecoes(componente)
    elemento.classList.add(CLASSE_SELECAO)
  }

  methods.removerSelecoes = (componente) => {
    const formatosSelecionados = componente.querySelectorAll(`.formato.${CLASSE_SELECAO}`)
    if (formatosSelecionados.length === 0) return

    formatosSelecionados[0].classList.remove(CLASSE_SELECAO)
  }

  // Pesquisa

  methods.obterButtonFormatoPorId = (componente, idFormato) => {
    return componente.querySelector(`.formato button[data-id="${idFormato}"]`)
  }

  return methods
}

module.exports = Module

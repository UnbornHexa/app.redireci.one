/* ---- Module ---- */

const Module = [
  { id: 1, formato: '1' },
  { id: 2, formato: '2' },
  { id: 3, formato: '3' },
  { id: 4, formato: '4' },
  { id: 5, formato: '5' },
  { id: 13, formato: '13' },
  { id: 14, formato: '14' },
  { id: 17, formato: '17' },
  { id: 18, formato: '18' },
  { id: 19, formato: '19' },
  { id: 20, formato: '20' },
  { id: 21, formato: '21' },
  { id: 29, formato: '29' },
  { id: 30, formato: '30' },
  { id: 33, formato: '33' },
  { id: 34, formato: '34' },
  { id: 35, formato: '35' },
  { id: 36, formato: '36' },
  { id: 37, formato: '37' },
  { id: 45, formato: '45' },
  { id: 46, formato: '46' },
  { id: 49, formato: '49' },
  { id: 50, formato: '50' },
  { id: 51, formato: '51' },
  { id: 52, formato: '52' },
  { id: 53, formato: '53' },
  { id: 61, formato: '61' },
  { id: 62, formato: '62' }
]

module.exports = Module

/* ---- Requires ---- */

const DATA = require('./data')
const EVENTS = require('./events')
const HELPERS = require('./helpers')
const VIEW = require('./view')

/* ---- Module ---- */

class AppListarBotoesFormatos extends window.HTMLElement {
  constructor () {
    super()
    this.__state = {
      selecionado: {
        id: '',
        formato: ''
      }
    }

    this.renderizar()
    EVENTS().clickFormato(this)
  }

  /* ---- Methods ---- */

  // Render

  renderizar () {
    const formatos = DATA
    for (const formato of formatos) {
      this.renderizarNovoFormato(formato)
    }
  }

  renderizarNovoFormato (dados) {
    const classeFormato = `formato_botao_${dados?.formato}`

    const formato = document.createElement('div')
    formato.classList.add('formato')
    formato.innerHTML = `<button class="${classeFormato}" data-id="${dados?.id}">Seu Botão</button>`
    this.appendChild(formato)
  }

  // Util

  selecionar (elemento, formatoObj) {
    if (!elemento || !formatoObj) return

    VIEW().adicionarClasseSelecao(this, elemento)

    this.__state.selecionado.id = formatoObj.id
    this.__state.selecionado.formato = formatoObj.formato
  }

  vazio () {
    return (!this.__state.selecionado.formato)
  }

  // Data

  importarFormatoSelecionado (nome) {
    const formatoObj = HELPERS().pesquisarFormatoPorNome(nome)
    if (!formatoObj) return

    const buttonFormato = VIEW().obterButtonFormatoPorId(this, formatoObj.id)
    const divPai = buttonFormato.parentElement

    this.selecionar(divPai, formatoObj)
  }

  exportarFormatoSelecionado () {
    return this.__state.selecionado.formato
  }
}

module.exports = AppListarBotoesFormatos

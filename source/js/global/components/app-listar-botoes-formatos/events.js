/* ---- Requires ---- */

const HELPERS = require('./helpers')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.clickFormato = (componente) => {
    componente.addEventListener('click', callbackClickFormato)
  }

  /* ---- Callbacks ---- */

  function callbackClickFormato (evento) {
    const elemento = '.formato'
    if (!evento.target.matches(elemento)) return

    const botao = evento.target.querySelector('button')
    const idFormato = botao.getAttribute('data-id')

    const formatoObj = HELPERS().pesquisarFormatoPorId(idFormato)
    this.selecionar(evento.target, formatoObj)
  }

  return methods
}

module.exports = Module

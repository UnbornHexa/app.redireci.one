/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.blockDragDrop = () => {
    document.addEventListener('dragstart', callbackBlockDragDrop, false)
    document.addEventListener('drop', callbackBlockDragDrop, false)
  }

  /* ---- Callbacks ---- */

  function callbackBlockDragDrop (evento) {
    evento.preventDefault()
  }

  return methods
}

module.exports = Module

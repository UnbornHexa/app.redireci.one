/* ---- Requires ---- */

const WINDOW = require('./window')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventosGlobais = () => {
    WINDOW().blockDragDrop()
  }

  return methods
}

module.exports = Module

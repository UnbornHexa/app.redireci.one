/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../helpers/redirect')
const VIEW = require('./view')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const nav = document.querySelector('nav')
  const buttonToggle = nav.querySelector('button[name="toggle"]')
  const logoVoltar = nav.querySelector('div[name="para_painel"]')

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    clickToggle()
    clickMenu()
    clickVoltar()
  }

  /* ---- Events ---- */

  function clickToggle () {
    buttonToggle.addEventListener('click', callbackClickToggle)
  }

  function clickMenu () {
    nav.addEventListener('click', callbackClickMenu)
  }

  function clickVoltar () {
    logoVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickToggle () {
    const estaAberto = VIEW().detectarAberto()

    if (estaAberto) VIEW().fecharMenu()
    else VIEW().abrirMenu()
  }

  function callbackClickMenu (evento) {
    const botao = 'div[name="menu"] button'
    if (!evento.target.matches(botao)) return

    const nomePagina = evento.target.name
    HELPER_REDIRECT().toPage(nomePagina)
  }

  function callbackClickVoltar () {
    HELPER_REDIRECT().toPage('telas')
  }

  return methods
}

module.exports = Module

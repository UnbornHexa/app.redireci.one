/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_MENU_MOSTRAR = 'mostrar'
  const CLASSE_MENU_ABRIR = 'abrir'
  const CLASSE_TOGGLE_FECHAR = 'fechar'

  /* ---- Elements ---- */

  const nav = document.querySelector('nav')
  const buttonToggle = nav.querySelector('button[name="toggle"]')
  const divMenu = nav.querySelector('div[name="menu"]')
  const pToggle = buttonToggle.querySelector('p')

  /* ---- Methods ---- */

  methods.abrirMenu = () => {
    divMenu.classList.add(CLASSE_MENU_MOSTRAR)
    divMenu.classList.remove(CLASSE_MENU_ABRIR)
    buttonToggle.classList.add(CLASSE_TOGGLE_FECHAR)
    pToggle.innerText = 'FECHAR'
  }

  methods.fecharMenu = () => {
    divMenu.classList.remove(CLASSE_MENU_MOSTRAR)
    divMenu.classList.add(CLASSE_MENU_ABRIR)
    buttonToggle.classList.remove(CLASSE_TOGGLE_FECHAR)
    pToggle.innerText = 'OPÇÕES'
  }

  methods.detectarAberto = () => {
    return (divMenu.classList.contains(CLASSE_MENU_MOSTRAR))
  }

  return methods
}

module.exports = Module

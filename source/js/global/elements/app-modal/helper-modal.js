/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_BODY_BLOQUEADO = 'bloqueado'
  const CLASSE_MODAL_ABERTO = 'mostrar'

  /* ---- Elements ---- */

  const body = document.querySelector('body')

  /* ---- Methods ---- */

  methods.abrirModal = (modal) => {
    body.classList.add(CLASSE_BODY_BLOQUEADO)
    modal.classList.add(CLASSE_MODAL_ABERTO)
  }

  methods.fecharModal = (modal) => {
    body.classList.remove(CLASSE_BODY_BLOQUEADO)
    modal.classList.remove(CLASSE_MODAL_ABERTO)
  }

  return methods
}

module.exports = Module

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const TOKEN_NAME = 'token-usuario'

  /* ---- Methods ---- */

  methods.getToken = () => {
    return window.localStorage.getItem(TOKEN_NAME) || false
  }

  methods.setToken = (data) => {
    window.localStorage.setItem(TOKEN_NAME, data)
  }

  methods.delToken = () => {
    window.localStorage.removeItem(TOKEN_NAME)
  }

  // Get Info From Token

  methods.readTimestamp = () => {
    return readToken(methods.getToken()).exp || 0
  }

  methods.readUsuarioId = () => {
    return readToken(methods.getToken()).data.idUsuario || 0
  }

  methods.readPaginaId = () => {
    return readToken(methods.getToken()).data.idPagina || 0
  }

  methods.readAssinaturaTipo = () => {
    return readToken(methods.getToken()).data.assinaturaTipo
  }

  /* ---- Aux Functions ---- */

  function readToken (token) {
    const payload = token.match(/\.([^.]+)\./)[1]
    const tokenDecrypted = b64DecodeUnicode(payload)
    return JSON.parse(tokenDecrypted)
  }

  function b64DecodeUnicode (str) {
    return decodeURIComponent(window.atob(str).split('').map(c => {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
  }

  return methods
}

module.exports = Module

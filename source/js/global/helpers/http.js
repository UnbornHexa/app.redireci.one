/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const HEADERS = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }

  /* ---- Methods ---- */

  methods.get = (url, headers, callback) => {
    const fetchOption = {
      method: 'GET',
      headers: headers || HEADERS
    }
    request(url, fetchOption, callback)
  }

  methods.post = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'POST',
      body: JSON.stringify(data) || JSON.stringify({}),
      headers: headers || HEADERS
    }
    request(url, fetchOption, callback)
  }

  methods.put = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'PUT',
      body: JSON.stringify(data) || JSON.stringify({}),
      headers: headers || HEADERS
    }
    request(url, fetchOption, callback)
  }

  methods.delete = (url, headers, callback) => {
    const fetchOption = {
      method: 'DELETE',
      headers: headers || HEADERS
    }
    request(url, fetchOption, callback)
  }

  methods.patch = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'PATCH',
      body: JSON.stringify(data) || JSON.stringify({}),
      headers: headers || HEADERS
    }
    request(url, fetchOption, callback)
  }

  // Custom Headers

  methods.setHeadersWithToken = (token) => {
    const headers = JSON.parse(JSON.stringify(HEADERS))
    headers['x-access-token'] = token
    return headers
  }

  /* ---- Aux Function ---- */

  function request (url, fetchOption, callback) {
    fetchOption = (fetchOption === null) ? {} : fetchOption

    window.fetch(url, fetchOption)
      .then(response => {
        response.json()
          .then(data => {
            const result = {
              status: response.status,
              body: data
            }
            callback(result)
          })
      })
      .catch(error => callback(error))
  }

  return methods
}

module.exports = Module

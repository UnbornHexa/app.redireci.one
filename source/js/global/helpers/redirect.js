/* ---- Requires ---- */

const HOSTS = require('../data/hosts')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.toPage = (pagina) => {
    const url = `${HOSTS().url.SSR_APP}/${pagina}`
    window.location.assign(url)
  }

  methods.toExternalPage = (url) => {
    if (!url) return

    const a = document.createElement('a')
    a.rel = 'noopener noreferrer nofollow'
    a.target = '_blank'
    a.href = url
    a.click()
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_REDIRECT = require('./redirect')
const HELPER_TOKEN = require('./token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.checkTokenOk = () => {
    if (!HELPER_TOKEN().getToken()) { methods.logout() }
    if (methods.tokenExpired()) { methods.logout() }
  }

  methods.logout = () => {
    HELPER_TOKEN().delToken()
    HELPER_REDIRECT().toPage('entrar')
  }

  // Aux Functions

  methods.tokenExpired = () => {
    const expiration = HELPER_TOKEN().readTimestamp()
    const now = Number(new Date().getTime() / 1000).toFixed(0)
    const diff = expiration - now

    return (diff < 0)
  }

  return methods
}

module.exports = Module

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.remove = (content) => {
    return content
      .replace(/[À|Á|Â|Ä|Ã]/ig, 'A')
      .replace(/[È|É|Ê|Ë|Ẽ]/ig, 'E')
      .replace(/[Ì|Í|Î|Ï|Ĩ]/ig, 'I')
      .replace(/[Ò|Ó|Ô|Ö|Õ]/ig, 'O')
      .replace(/[Ù|Ú|Û|Ü|Ũ]/ig, 'U')
      .replace(/[Ç]/ig, 'C')
      .replace(/[Ñ]/ig, 'N')
      .replace(/[\n]/ig, ' ')
  }

  return methods
}

module.exports = Module

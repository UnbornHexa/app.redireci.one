/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.postFile = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'POST',
      body: data,
      headers: headers || {}
    }
    request(url, fetchOption, callback)
  }

  // Custom Headers

  methods.setHeadersWithToken = (token) => {
    return { 'x-access-token': token }
  }

  /* ---- Aux Function ---- */

  function request (url, fetchOption, callback) {
    fetchOption = (fetchOption === null) ? {} : fetchOption

    window.fetch(url, fetchOption)
      .then(response => {
        response.json()
          .then(data => {
            const result = {
              status: response.status,
              body: data
            }
            callback(result)
          })
      })
      .catch(error => callback(error))
  }

  return methods
}

module.exports = Module

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Contants ---- */

  const PROTOCOL = window.location.protocol
  const HOST = window.location.host

  /* ---- Methods ---- */

  methods.url = {
    SSR_APP: `${PROTOCOL}//${HOST}`,

    API_APP: 'https://api-app.redireci.one',
    // API_APP: 'http://localhost:53100',

    API_PAY: 'https://api-pay.redireci.one',
    // API_PAY: 'http://localhost:53102',

    API_UPLOAD: 'https://api-upload.redireci.one'
    // API_UPLOAD: 'http://localhost:53104'
  }

  return methods
}

module.exports = Module

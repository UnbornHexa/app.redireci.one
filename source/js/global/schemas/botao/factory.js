/* ---- Requires ---- */

const BotaoModel = require('./model')

/* ---- Module ---- */

const BotaoFactory = (propriedades) => {
  const botao = new BotaoModel(
    propriedades.formato,
    propriedades.corFundo,
    propriedades.corTexto,
    propriedades.texto,
    propriedades.fonte,
    propriedades.url
  )

  Object.freeze(botao)
  return botao
}

module.exports = BotaoFactory

/* ---- Module ---- */

class BotaoModel {
  constructor (
    formato,
    corFundo,
    corTexto,
    texto,
    fonte,
    url
  ) {
    this.formato = formato || '17'
    this.corFundo = corFundo || '#ffffff'
    this.corTexto = corTexto || '#000000'
    this.texto = texto || 'Seu Botão'
    this.fonte = fonte || 'gilroy_bold'
    this.url = url || ''
  }
}

module.exports = BotaoModel

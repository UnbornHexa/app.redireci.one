/* ---- Module ---- */

class FundoModel {
  constructor (
    selecionado,
    cor,
    imagem,
    gradienteCor1,
    gradienteCor2
  ) {
    this.selecionado = selecionado || 'imagem'
    this.cor = cor || ''
    this.imagem = imagem || 'https://static.redireci.one/banco/fundos/fundo-70.png'

    this.gradiente = {}
    this.gradiente.cor1 = gradienteCor1 || ''
    this.gradiente.cor2 = gradienteCor2 || ''
  }
}

module.exports = FundoModel

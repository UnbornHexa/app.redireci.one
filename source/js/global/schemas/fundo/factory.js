/* ---- Requires ---- */

const FundoModel = require('./model')

/* ---- Module ---- */

const FundoFactoryCor = (propriedades) => {
  const fundo = new FundoModel(
    'cor',
    propriedades.cor,
    null,
    null,
    null
  )

  Object.freeze(fundo)
  return fundo
}

/* ---- Module ---- */

const FundoFactoryImagem = (propriedades) => {
  const fundo = new FundoModel(
    'imagem',
    null,
    propriedades.imagem,
    null,
    null
  )

  Object.freeze(fundo)
  return fundo
}

/* ---- Module ---- */

const FundoFactoryGradiente = (propriedades) => {
  const fundo = new FundoModel(
    'gradiente',
    null,
    null,
    propriedades.cor1,
    propriedades.cor2
  )

  Object.freeze(fundo)
  return fundo
}

module.exports = {
  FundoFactoryCor,
  FundoFactoryImagem,
  FundoFactoryGradiente
}

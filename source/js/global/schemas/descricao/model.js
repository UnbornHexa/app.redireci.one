/* ---- Module ---- */

class DescricaoModel {
  constructor (
    fonte,
    cor,
    texto
  ) {
    this.fonte = fonte || 'cyntho_next_regular'
    this.cor = cor || '#ffffff'
    this.texto = texto || 'Essa é uma descrição modelo, você pode escrever o que quiser aqui.'
  }
}

module.exports = DescricaoModel

/* ---- Requires ---- */

const DescricaoModel = require('./model')

/* ---- Module ---- */

const DescricaoFactory = (propriedades) => {
  const descricao = new DescricaoModel(
    propriedades.fonte,
    propriedades.cor,
    propriedades.texto
  )

  Object.freeze(descricao)
  return descricao
}

module.exports = DescricaoFactory

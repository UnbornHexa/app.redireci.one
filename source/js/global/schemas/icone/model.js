/* ---- Module ---- */

class IconeModel {
  constructor (
    formato,
    imagem
  ) {
    this.formato = formato || 'quadrado'
    this.imagem = imagem || 'https://static.redireci.one/banco/icones/redes-sociais/facebook-8.svg'
  }
}

module.exports = IconeModel

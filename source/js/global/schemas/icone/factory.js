/* ---- Requires ---- */

const IconeModel = require('./model')

/* ---- Module ---- */

const IconeFactory = (propriedades) => {
  const icone = new IconeModel(
    propriedades.formato,
    propriedades.imagem
  )

  Object.freeze(icone)
  return icone
}

module.exports = IconeFactory

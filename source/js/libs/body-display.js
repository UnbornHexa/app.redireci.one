(function () {
  /* ---- Elements ---- */

  const body = document.querySelector('body')

  /* ---- Events ---- */

  window.addEventListener('load', () => callbackExibirBody())

  /* ---- Callbacks ---- */

  function callbackExibirBody () {
    body.style.display = 'block'
  }
}())

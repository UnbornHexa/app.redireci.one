(function () {
  /* ---- Requires ---- */

  const HELPER_TOKEN = require('../global/helpers/token')

  /* ---- Constants ---- */

  const ID_APP = '5fd5c2617074fe22c9d52379'
  const ID_CLIENTE = HELPER_TOKEN().readUsuarioId()
  const ID_PAGINA = HELPER_TOKEN().readPaginaId()

  /* --- Events ---- */

  window.addEventListener('load', () => callbackAbrirChaport())

  /* ---- Callbacks ---- */

  function callbackAbrirChaport () {
    window.chaportConfig = {
      appId: ID_APP,
      visitor: {
        language: 'string',
        custom: {
          idCliente: ID_CLIENTE,
          idPagina: ID_PAGINA
        }
      },
      launcher: {
        show: false
      }
    }

    configurarChaport()
    baixarScriptJS()
  }

  /* ---- Aux Functions ---- */

  function configurarChaport () {
    if (window.chaport) return

    const v3 = window.chaport = {}
    v3._q = []
    v3._l = {}
    v3.q = function () { v3._q.push(arguments) }

    v3.on = function (elemento, funcao) {
      if (!v3._l[elemento]) { v3._l[elemento] = [] }
      v3._l[elemento].push(funcao)
    }
  }

  function baixarScriptJS () {
    const novoScript = document.createElement('script')
    novoScript.type = 'text/javascript'
    novoScript.async = true
    novoScript.src = 'https://app.chaport.com/javascripts/insert.js'

    const primeiroScript = document.getElementsByTagName('script')[0]
    primeiroScript.parentNode.insertBefore(novoScript, primeiroScript)
  }
}())

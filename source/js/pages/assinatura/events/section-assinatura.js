/* ---- Requires ---- */

const REQUEST_STRIPE = require('../../../global/requests/stripe')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="assinatura"]')
  const buttonAssinar = section.querySelector('button[name="assinar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickAssinar()
  }

  function clickAssinar () {
    buttonAssinar.addEventListener('click', callbackClickAssinar)
  }

  /* ---- Callbacks ---- */

  function callbackClickAssinar () {
    buttonAssinar.setAttribute('disabled', true)

    REQUEST_STRIPE().setup()
      .then(token => {
        REQUEST_STRIPE().criarSession()
          .then(session => {
            const stripe = window.Stripe(token)
            stripe.redirectToCheckout({ sessionId: session.id })
          })
          .finally(() => buttonAssinar.removeAttribute('disabled'))
      })
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_TOKEN = require('../../../global/helpers/token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_ASSINANTE = 'assinante'

  /* ---- Elements ---- */

  const buttonNavAssinatura = document.querySelector('nav button[name="assinatura"]')

  /* ---- Methods ---- */

  methods.detectarAssinante = () => {
    const tipoAssinatura = HELPER_TOKEN().readAssinaturaTipo()
    if (tipoAssinatura !== 'gratis') modificarElementos()
  }

  /* ---- Aux Function ---- */

  function modificarElementos () {
    buttonNavAssinatura.classList.add(CLASSE_ASSINANTE)
  }

  return methods
}

module.exports = Module

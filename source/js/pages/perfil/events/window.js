/* ---- Requires ---- */

const HELPER_POVOAR_INFORMACOES_USUARIO = require('../helpers/povoar-informacoes-usuario')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.povoar = () => {
    window.addEventListener('load', callbackPovoar)
  }

  /* ---- Callbacks ---- */

  function callbackPovoar () {
    HELPER_POVOAR_INFORMACOES_USUARIO().povoar()
  }

  return methods
}

module.exports = Module

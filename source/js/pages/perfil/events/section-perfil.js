/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../../global/helpers/redirect')
const VIEW_MODAL_SAIR = require('../views/app-modal-sair')
const VIEW_MODAL_SENHA = require('../views/app-modal-senha')
const VIEW_MODAL_USUARIO = require('../views/app-modal-usuario')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="perfil"]')
  const pUrl = section.querySelector('p[name="url"]')

  const buttonAssinar = section.querySelector('button[name="assinar"]')
  const buttonAlterarSenha = section.querySelector('button[name="alterar_senha"]')
  const buttonAlterarUsuario = section.querySelector('button[name="alterar_usuario"]')
  const buttonSair = section.querySelector('button[name="sair"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickUrl()
    clickAssinar()
    clickAlterarSenha()
    clickAlterarUsuario()
    clickSair()
  }

  function clickUrl () {
    pUrl.addEventListener('click', callbackClickUrl)
  }

  function clickAssinar () {
    buttonAssinar.addEventListener('click', callbackClickAssinar)
  }

  function clickAlterarSenha () {
    buttonAlterarSenha.addEventListener('click', callbackClickAlterarSenha)
  }

  function clickAlterarUsuario () {
    buttonAlterarUsuario.addEventListener('click', callbackClickAlterarUsuario)
  }

  function clickSair () {
    buttonSair.addEventListener('click', callbackClickSair)
  }

  /* ---- Callbacks ---- */

  function callbackClickUrl () {
    const url = pUrl.innerText
    HELPER_REDIRECT().toExternalPage(url)
  }

  function callbackClickAssinar () {
    HELPER_REDIRECT().toPage('assinatura')
  }

  function callbackClickAlterarSenha () {
    VIEW_MODAL_SENHA().mostrar()
  }

  function callbackClickAlterarUsuario () {
    VIEW_MODAL_USUARIO().mostrar()
  }

  function callbackClickSair () {
    VIEW_MODAL_SAIR().mostrar()
  }

  return methods
}

module.exports = Module

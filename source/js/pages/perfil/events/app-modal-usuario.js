/* ---- Requires ---- */

const HELPER_POVOAR_INFORMACOES_USUARIO = require('../helpers/povoar-informacoes-usuario')
const REQUEST_USUARIOS = require('../../../global/requests/usuarios')
const VIEW_MODAL_USUARIO = require('../views/app-modal-usuario')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="alterar_usuario"]')

  const buttonAlterar = appModal.querySelector('button[name="alterar"]')
  const buttonVoltar = appModal.querySelector('button[name="voltar"]')
  const inputUsuarioNovo = appModal.querySelector('input[name="usuario_novo"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickAlterar()
    clickVoltar()
    InputBlockCaracteresUsuario()
  }

  function clickAlterar () {
    buttonAlterar.addEventListener('click', callbackClickAlterar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  function InputBlockCaracteresUsuario () {
    inputUsuarioNovo.addEventListener('keyup', callbackInputBlockCaracteresUsuario)
  }

  /* ---- Callbacks ---- */

  function callbackClickAlterar () {
    const dados = VIEW_MODAL_USUARIO().exportarDados()

    const camposObrigatoriosOk = VIEW_MODAL_USUARIO().verificarCamposObrigatorios()
    if (!camposObrigatoriosOk) return

    buttonAlterar.setAttribute('disabled', true)
    REQUEST_USUARIOS().editarUsuario(dados)
      .then(() => {
        HELPER_POVOAR_INFORMACOES_USUARIO().povoar()
        VIEW_MODAL_USUARIO().esconder()
      })
      .finally(() => buttonAlterar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_USUARIO().esconder()
  }

  function callbackInputBlockCaracteresUsuario (evento) {
    const usuarioDigitado = evento.target.value
    const regexFiltro = /[^a-z|0-9|_.-]/gi
    const usuarioFiltrado = usuarioDigitado.replace(regexFiltro, '')
    evento.target.value = usuarioFiltrado
  }

  return methods
}

module.exports = Module

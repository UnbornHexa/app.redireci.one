/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_MODAL_SAIR = require('./app-modal-sair')
const EVENT_MODAL_SENHA = require('./app-modal-senha')
const EVENT_MODAL_PERFIL = require('./app-modal-usuario')
const EVENT_NAV = require('../../../global/elements/nav/events')
const EVENT_SECTION_PERFIL = require('./section-perfil')
const EVENT_WINDOW = require('./window')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()
    EVENT_NAV().habilitarTodosOsEventos()

    EVENT_WINDOW().povoar()

    EVENT_SECTION_PERFIL().habilitarEventos()
    EVENT_MODAL_SAIR().habilitarEventos()
    EVENT_MODAL_SENHA().habilitarEventos()
    EVENT_MODAL_PERFIL().habilitarEventos()
  }

  return methods
}

module.exports = Module

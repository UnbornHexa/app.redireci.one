/* ---- Requires ---- */

const REQUEST_USUARIOS = require('../../../global/requests/usuarios')
const VIEW_MODAL_SENHA = require('../views/app-modal-senha')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="alterar_senha"]')

  const buttonAlterar = appModal.querySelector('button[name="alterar"]')
  const buttonVoltar = appModal.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickAlterar()
    clickVoltar()
  }

  function clickAlterar () {
    buttonAlterar.addEventListener('click', callbackClickAlterar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickAlterar () {
    const camposObrigatoriosOk = VIEW_MODAL_SENHA().verificarCamposObrigatorios()
    if (!camposObrigatoriosOk) return

    const senhasIguaisOk = VIEW_MODAL_SENHA().verificarSenhaIguais()
    if (!senhasIguaisOk) return

    const senhaForcaMinimaOk = VIEW_MODAL_SENHA().verificarSenhaForcaMinima()
    if (!senhaForcaMinimaOk) return

    const dados = VIEW_MODAL_SENHA().exportarDados()
    buttonAlterar.setAttribute('disabled', true)
    REQUEST_USUARIOS().editarSenha(dados)
      .then(() => VIEW_MODAL_SENHA().esconder())
      .finally(() => buttonAlterar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_SENHA().esconder()
  }

  return methods
}

module.exports = Module

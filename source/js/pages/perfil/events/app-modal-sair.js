/* ---- Requires ---- */

const HELPER_LOGOUT = require('../../../global/helpers/logout')
const VIEW_MODAL_SAIR = require('../views/app-modal-sair')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="sair_sistema"]')

  const buttonSair = appModal.querySelector('button[name="sair"]')
  const buttonVoltar = appModal.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickSair()
    clickVoltar()
  }

  /* ---- Events ---- */

  function clickSair () {
    buttonSair.addEventListener('click', callbackClickSair)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickSair () {
    HELPER_LOGOUT().logout()
  }

  function callbackClickVoltar () {
    VIEW_MODAL_SAIR().esconder()
  }

  return methods
}

module.exports = Module

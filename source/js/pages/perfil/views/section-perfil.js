/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="perfil"]')
  const appModal = document.querySelector('app-modal[name="alterar_usuario"]')

  const pUrl = section.querySelector('p[name="url"]')
  const pEmail = section.querySelector('p[name="email"]')
  const pUrlUsuario = appModal.querySelector('p[name="usuario_atual"]')

  /* ---- Methods ---- */

  methods.importarDados = (dados) => {
    pUrl.innerText = `https://redireci.one/${dados?.usuario}`
    pUrlUsuario.innerText = `redireci.one/${dados?.usuario}`
    pEmail.innerText = dados?.email
  }

  return methods
}

module.exports = Module

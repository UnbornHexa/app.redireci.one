/* ---- Requires ---- */

const HELPER_MODAL = require('../../../global/elements/app-modal/helper-modal')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="alterar_usuario"]')
  const appAlerta = document.querySelector('app-alerta')
  const inputUsuarioNovo = appModal.querySelector('input[name="usuario_novo"]')

  /* ---- Methods ---- */

  // Modal

  methods.mostrar = () => {
    methods.limparCampos()
    HELPER_MODAL().abrirModal(appModal)
  }

  methods.esconder = () => {
    HELPER_MODAL().fecharModal(appModal)
  }

  // Form

  methods.limparCampos = () => {
    inputUsuarioNovo.value = ''
  }

  methods.verificarCamposObrigatorios = () => {
    if (!inputUsuarioNovo.value) appAlerta.alertar('Digite sua nova url.')
    else return true

    return false
  }

  // Data

  methods.exportarDados = () => {
    const usuario = {
      usuario: inputUsuarioNovo.value
    }
    return usuario
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_MODAL = require('../../../global/elements/app-modal/helper-modal')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="sair_sistema"]')

  /* ---- Methods ---- */

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL().abrirModal(appModal)
  }

  methods.esconder = () => {
    HELPER_MODAL().fecharModal(appModal)
  }

  return methods
}

module.exports = Module

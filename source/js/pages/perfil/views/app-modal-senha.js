/* ---- Requires ---- */

const HELPER_MODAL = require('../../../global/elements/app-modal/helper-modal')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="alterar_senha"]')
  const appAlerta = document.querySelector('app-alerta')

  const inputSenhaAtual = appModal.querySelector('input[name="senha_atual"]')
  const inputSenhaNova = appModal.querySelector('input[name="senha_nova"]')
  const inputSenhaNova2 = appModal.querySelector('input[name="senha_nova2"]')

  /* ---- Methods ---- */

  // Modal

  methods.mostrar = () => {
    methods.limparCampos()
    HELPER_MODAL().abrirModal(appModal)
  }

  methods.esconder = () => {
    HELPER_MODAL().fecharModal(appModal)
  }

  // Form

  methods.limparCampos = () => {
    inputSenhaAtual.value = ''
    inputSenhaNova.value = ''
    inputSenhaNova2.value = ''
  }

  methods.verificarCamposObrigatorios = () => {
    if (!inputSenhaAtual.value) appAlerta.alertar('Digite sua senha atual.')
    else if (!inputSenhaNova.value) appAlerta.alertar('Digite uma nova senha.')
    else if (!inputSenhaNova2.value) appAlerta.alertar('Confirme sua nova senha.')
    else return true

    return false
  }

  methods.verificarSenhaIguais = () => {
    if (inputSenhaNova.value !== inputSenhaNova2.value) appAlerta.alertar('As senhas devem ser iguais.')
    else return true

    return false
  }

  methods.verificarSenhaForcaMinima = () => {
    if (inputSenhaNova.value.length < 6) appAlerta.alertar('A senha deve ter no mínimo 6 caracteres.')
    else return true

    return false
  }

  // Data

  methods.exportarDados = () => {
    const usuario = {
      senhaAtual: inputSenhaAtual.value,
      senhaNova: inputSenhaNova.value
    }
    return usuario
  }

  return methods
}

module.exports = Module

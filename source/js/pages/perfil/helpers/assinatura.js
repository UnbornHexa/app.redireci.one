/* ---- Requires ---- */

const HELPER_TOKEN = require('../../../global/helpers/token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_ASSINANTE = 'assinante'

  /* ---- Elements ---- */

  const buttonAssinar = document.querySelector('button[name="assinar"]')
  const buttonNavAssinatura = document.querySelector('nav button[name="assinatura"]')
  const spanTipo = document.querySelector('p[name="tipo_assinatura"] span')
  const imgAssinante = document.querySelector('img[name="imagem_assinante"]')
  const imgGratis = document.querySelector('img[name="imagem_gratis"]')

  /* ---- Methods ---- */

  methods.detectarAssinante = () => {
    const tipoAssinatura = HELPER_TOKEN().readAssinaturaTipo()
    if (tipoAssinatura !== 'gratis') modificarElementos(tipoAssinatura)
  }

  /* ---- Aux Function ---- */

  function modificarElementos (tipoAssinatura) {
    const tipo = tipoAssinatura.toString().toUpperCase()
    spanTipo.innerText = tipo

    buttonAssinar.classList.add(CLASSE_ASSINANTE)
    buttonNavAssinatura.classList.add(CLASSE_ASSINANTE)
    imgGratis.classList.add(CLASSE_ASSINANTE)
    imgAssinante.classList.add(CLASSE_ASSINANTE)
    spanTipo.classList.add(CLASSE_ASSINANTE)
  }

  return methods
}

module.exports = Module

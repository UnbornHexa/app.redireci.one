/* ---- Requires ---- */

const REQUEST_USUARIO = require('../../../global/requests/usuarios')
const VIEW_SECTION_PERFIL = require('../views/section-perfil')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.povoar = () => {
    REQUEST_USUARIO().receberPorId()
      .then(usuario => VIEW_SECTION_PERFIL().importarDados(usuario))
  }

  return methods
}

module.exports = Module

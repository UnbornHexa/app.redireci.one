/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../../global/helpers/redirect')
const HELPER_TOKEN = require('../../../global/helpers/token')
const REQUEST_AUTENTICACAO = require('../../../global/requests/autenticacao')
const VIEW_SECTION_REGISTRAR = require('../views/section-registrar')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.autenticar = () => {
    const dados = VIEW_SECTION_REGISTRAR().exportarAutenticacao()
    REQUEST_AUTENTICACAO().entrar(dados)
      .then(token => HELPER_TOKEN().setToken(token))
      .then(() => HELPER_REDIRECT().toPage('telas'))
  }

  return methods
}

module.exports = Module

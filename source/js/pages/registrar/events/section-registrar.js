/* ---- Requires ---- */

const HELPER_AUTO_AUTENTICACAO = require('../helpers/auto-autenticacao')
const HELPER_REDIRECT = require('../../../global/helpers/redirect')
const REQUEST_AUTENTICACAO = require('../../../global/requests/autenticacao')
const VIEW_SECTION_REGISTRAR = require('../views/section-registrar')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="registrar"]')
  const inputUsuario = section.querySelector('input[name="usuario"]')
  const buttonRegistrar = section.querySelector('button[name="registrar"]')
  const buttonEntrar = section.querySelector('button[name="entrar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickRegistrar()
    clickEntrar()
    inputBlockCaracteresUsuario()
  }

  function clickRegistrar () {
    buttonRegistrar.addEventListener('click', callbackClickRegistrar)
  }

  function clickEntrar () {
    buttonEntrar.addEventListener('click', callbackClickEntrar)
  }

  function inputBlockCaracteresUsuario () {
    inputUsuario.addEventListener('input', callbackInputBlockCaracteresUsuario)
  }

  /* ---- Callbacks ---- */

  function callbackClickRegistrar () {
    const dados = VIEW_SECTION_REGISTRAR().exportarDados()

    const camposObrigatoriosOk = VIEW_SECTION_REGISTRAR().verificarCamposObrigatorios()
    if (!camposObrigatoriosOk) return false

    const senhasIguaisOk = VIEW_SECTION_REGISTRAR().verificarSenhaIguais()
    if (!senhasIguaisOk) return false

    const senhaForcaMinimaOk = VIEW_SECTION_REGISTRAR().verificarSenhaForcaMinima()
    if (!senhaForcaMinimaOk) return false

    buttonRegistrar.setAttribute('disabled', true)
    REQUEST_AUTENTICACAO().registrar(dados)
      .then(() => HELPER_AUTO_AUTENTICACAO().autenticar())
      .finally(() => buttonRegistrar.removeAttribute('disabled'))
  }

  function callbackClickEntrar () {
    HELPER_REDIRECT().toPage('entrar')
  }

  function callbackInputBlockCaracteresUsuario (evento) {
    const usuarioDigitado = evento.target.value
    const regexFiltro = /[^a-z|0-9|_.-]/gi
    const usuarioFiltrado = usuarioDigitado.replace(regexFiltro, '')
    evento.target.value = usuarioFiltrado
  }

  return methods
}

module.exports = Module

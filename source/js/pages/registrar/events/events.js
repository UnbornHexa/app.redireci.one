/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_SECTION_REGISTRAR = require('./section-registrar')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()

    EVENT_SECTION_REGISTRAR().habilitarEventos()
  }

  return methods
}

module.exports = Module

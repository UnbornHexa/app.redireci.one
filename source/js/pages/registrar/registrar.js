/* ---- Requires ---- */

const APP_ALERTA = require('../../global/components/app-alerta/init')
const EVENTS = require('./events/events')

/* ---- Start ---- */

EVENTS().habilitarTodosOsEventos()

/* ---- Components ---- */

APP_ALERTA().iniciar()

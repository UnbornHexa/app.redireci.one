/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="registrar"]')
  const appAlerta = document.querySelector('app-alerta')

  const inputNome = section.querySelector('input[name="nome"]')
  const inputEmail = section.querySelector('input[name="email"]')
  const inputSenha = section.querySelector('input[name="senha"]')
  const inputSenha2 = section.querySelector('input[name="senha2"]')
  const inputUsuario = section.querySelector('input[name="usuario"]')

  /* ---- Methods ---- */

  // Form

  methods.limparCampos = () => {
    inputNome.value = ''
    inputEmail.value = ''
    inputSenha.value = ''
    inputSenha2.value = ''
    inputUsuario.value = ''
  }

  methods.verificarCamposObrigatorios = () => {
    if (!inputNome.value) appAlerta.alertar('Digite o seu nome.')
    else if (!inputEmail.value) appAlerta.alertar('Digite o seu email.')
    else if (!inputSenha.value) appAlerta.alertar('Digite uma senha.')
    else if (!inputSenha2.value) appAlerta.alertar('Confirme a senha que você digitou.')
    else if (!inputUsuario.value) appAlerta.alertar('Digite o seu usuário.')
    else return true

    return false
  }

  methods.verificarSenhaIguais = () => {
    if (inputSenha.value !== inputSenha2.value) appAlerta.alertar('As senhas devem ser iguais.')
    else return true

    return false
  }

  methods.verificarSenhaForcaMinima = () => {
    if (inputSenha.value.length < 6) appAlerta.alertar('A senha deve ter no mínimo 6 caracteres.')
    else return true

    return false
  }

  // Data

  methods.exportarDados = () => {
    const autenticacao = {
      nome: inputNome.value,
      email: inputEmail.value,
      senha: inputSenha.value,
      usuario: inputUsuario.value
    }
    return autenticacao
  }

  methods.exportarAutenticacao = () => {
    const autenticacao = {
      email: inputEmail.value,
      senha: inputSenha.value
    }
    return autenticacao
  }

  return methods
}

module.exports = Module

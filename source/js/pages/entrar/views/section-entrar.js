/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="entrar"]')
  const appAlerta = document.querySelector('app-alerta')

  const inputEmail = section.querySelector('input[name="email"]')
  const inputSenha = section.querySelector('input[name="senha"]')

  /* ---- Methods ---- */

  // Form

  methods.limparCampos = () => {
    inputEmail.value = ''
    inputSenha.value = ''
  }

  methods.verificarCamposObrigatorios = () => {
    if (!inputEmail.value) appAlerta.alertar('Digite seu email.')
    else if (!inputSenha.value) appAlerta.alertar('Digite sua senha.')
    else return true

    return false
  }

  // Data

  methods.exportarDados = () => {
    const autenticacao = {
      email: inputEmail.value,
      senha: inputSenha.value
    }
    return autenticacao
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../../global/helpers/redirect')
const HELPER_TOKEN = require('../../../global/helpers/token')
const REQUEST_AUTENTICACAO = require('../../../global/requests/autenticacao')
const VIEW_SECTION_ENTRAR = require('../views/section-entrar')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="entrar"]')
  const inputSenha = section.querySelector('input[name="senha"]')

  const buttonEntrar = section.querySelector('button[name="entrar"]')
  const buttonRegistrar = section.querySelector('button[name="registrar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickEntrar()
    clickRegistrar()
    keyEntrar()
  }

  function clickEntrar () {
    buttonEntrar.addEventListener('click', callbackClickEntrar)
  }

  function clickRegistrar () {
    buttonRegistrar.addEventListener('click', callbackClickRegistrar)
  }

  function keyEntrar () {
    inputSenha.addEventListener('keypress', callbackKeyEntrar)
  }

  /* ---- Callbacks ---- */

  function callbackClickEntrar () {
    const dados = VIEW_SECTION_ENTRAR().exportarDados()

    const camposObrigatoriosOk = VIEW_SECTION_ENTRAR().verificarCamposObrigatorios()
    if (!camposObrigatoriosOk) return false

    buttonEntrar.setAttribute('disabled', true)
    REQUEST_AUTENTICACAO().entrar(dados)
      .then(token => {
        HELPER_TOKEN().setToken(token)
        HELPER_REDIRECT().toPage('telas')
      })
      .finally(() => buttonEntrar.removeAttribute('disabled'))
  }

  function callbackClickRegistrar () {
    HELPER_REDIRECT().toPage('registrar')
  }

  function callbackKeyEntrar (evento) {
    const keyCode = evento.keyCode || evento.which
    if (keyCode !== 13) return false

    buttonEntrar.click()
  }

  return methods
}

module.exports = Module

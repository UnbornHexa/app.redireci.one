/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_SECTION_ENTRAR = require('./section-entrar')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()

    EVENT_SECTION_ENTRAR().habilitarEventos()
  }

  return methods
}

module.exports = Module

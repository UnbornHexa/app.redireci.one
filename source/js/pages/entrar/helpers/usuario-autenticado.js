/* ---- Requires ---- */

const HELPER_LOGOUT = require('../../../global/helpers/logout')
const HELPER_REDIRECT = require('../../../global/helpers/redirect')
const HELPER_TOKEN = require('../../../global/helpers/token')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.redirecionar = () => {
    const existeToken = HELPER_TOKEN().getToken()
    if (!existeToken) return

    const tokenExpirado = HELPER_LOGOUT().tokenExpired()
    if (tokenExpirado) return

    HELPER_REDIRECT().toPage('telas')
  }

  return methods
}

module.exports = Module

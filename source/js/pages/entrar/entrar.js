/* ---- Requires ---- */

const APP_ALERTA = require('../../global/components/app-alerta/init')
const EVENTS = require('./events/events')
const HELPER_USUARIO_AUTENTICADO = require('./helpers/usuario-autenticado')

/* ---- Start ---- */

EVENTS().habilitarTodosOsEventos()
HELPER_USUARIO_AUTENTICADO().redirecionar()

/* ---- Components ---- */

APP_ALERTA().iniciar()

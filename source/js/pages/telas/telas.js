/* ---- Requires ---- */

const APP_ALERTA = require('../../global/components/app-alerta/init')
const APP_LISTAR_TELAS = require('../../global/components/app-listar-telas/init')
const EVENTS = require('./events/events')
const HELPER_ASSINATURA = require('./helpers/assinatura')
const HELPER_LOGOUT = require('../../global/helpers/logout')

/* ---- Start ---- */

HELPER_LOGOUT().checkTokenOk()
HELPER_ASSINATURA().detectarAssinante()
EVENTS().habilitarTodosOsEventos()

/* ---- Components ---- */

APP_ALERTA().iniciar()
APP_LISTAR_TELAS().iniciar()

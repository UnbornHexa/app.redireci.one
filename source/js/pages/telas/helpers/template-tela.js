/* ---- Requires ---- */

const FACTORY_BOTAO = require('../../../global/schemas/botao/factory')
const FACTORY_DESCRICAO = require('../../../global/schemas/descricao/factory')
const FACTORY_FUNDO = require('../../../global/schemas/fundo/factory')
const FACTORY_ICONE = require('../../../global/schemas/icone/factory')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.criarTelaTemplate = () => {
    const nome = 'Nova Tela'
    const botao = criarBotaoPadrao()
    const descricao = criarDescricaoPadrao()
    const fundo = criarFundoPadrao()
    const icone = criarIconePadrao()

    return { nome, descricao, icone, botao, fundo }
  }

  /* ---- Aux Functions ---- */

  function criarBotaoPadrao () {
    const propriedades = {
      formato: null,
      corFundo: null,
      corTexto: null,
      texto: null,
      fonte: null,
      url: null
    }
    return new FACTORY_BOTAO(propriedades)
  }

  function criarDescricaoPadrao () {
    const propriedades = {
      fonte: null,
      cor: null,
      texto: null
    }
    return new FACTORY_DESCRICAO(propriedades)
  }

  function criarFundoPadrao () {
    const propriedades = { imagem: null }
    return new FACTORY_FUNDO.FundoFactoryImagem(propriedades)
  }

  function criarIconePadrao () {
    const propriedades = {
      formato: null,
      imagem: null
    }
    return new FACTORY_ICONE(propriedades)
  }

  return methods
}

module.exports = Module

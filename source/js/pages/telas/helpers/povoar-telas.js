/* ---- Requires ---- */

const REQUEST_TELAS = require('../../../global/requests/telas')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appListarTelas = document.querySelector('app-listar-telas')

  /* ---- Methods ---- */

  methods.povoar = () => {
    REQUEST_TELAS().receberTodos()
      .then(telas => appListarTelas.importarTelas(telas))
  }

  return methods
}

module.exports = Module

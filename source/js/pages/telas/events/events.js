/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_NAV = require('../../../global/elements/nav/events')
const EVENT_SECTION_TELAS = require('./section-telas')
const EVENT_WINDOW = require('./window')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()
    EVENT_NAV().habilitarTodosOsEventos()

    EVENT_WINDOW().loadPagina()
    EVENT_SECTION_TELAS().habilitarEventos()
  }

  return methods
}

module.exports = Module

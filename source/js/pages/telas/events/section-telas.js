/* ---- Requires ---- */

const HELPER_POVOAR_TELAS = require('../helpers/povoar-telas')
const HELPER_REDIRECT = require('../../../global/helpers/redirect')
const HELPER_TEMPLATE_TELAS = require('../helpers/template-tela')
const REQUEST_TELAS = require('../../../global/requests/telas')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="telas"]')
  const appListarTelas = section.querySelector('app-listar-telas')
  const pUrl = section.querySelector('p[name="url"]')
  const buttonAdicionar = section.querySelector('button[name="adicionar"]')
  const buttonAssinar = section.querySelector('button[name="assinar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickAdicionar()
    clickAssinar()
    clickUrl()
  }

  /* ---- Events ---- */

  function clickAdicionar () {
    buttonAdicionar.addEventListener('click', callbackClickAdicionar)
  }

  function clickAssinar () {
    buttonAssinar.addEventListener('click', callbackClickAssinar)
  }

  function clickUrl () {
    pUrl.addEventListener('click', callbackClickUrl)
  }

  /* ---- Callbacks ---- */

  function callbackClickAdicionar () {
    const novaTela = HELPER_TEMPLATE_TELAS().criarTelaTemplate()
    novaTela.posicao = Number(appListarTelas.children.length)

    buttonAdicionar.setAttribute('disabled', true)
    REQUEST_TELAS().adicionar(novaTela)
      .then(() => HELPER_POVOAR_TELAS().povoar())
      .finally(() => buttonAdicionar.removeAttribute('disabled'))
  }

  function callbackClickAssinar () {
    HELPER_REDIRECT().toPage('assinatura')
  }

  function callbackClickUrl () {
    const url = pUrl.innerText
    HELPER_REDIRECT().toExternalPage(url)
  }

  return methods
}

module.exports = Module

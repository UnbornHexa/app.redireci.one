/* ---- Requires ---- */

const HELPER_POVOAR_TELAS = require('../helpers/povoar-telas')
const REQUEST_PAGINAS = require('../../../global/requests/paginas')
const REQUEST_USUARIO = require('../../../global/requests/usuarios')
const VIEW_VISUALIZACOES = require('../views/app-visualizacoes')
const VIEW_SECTION_TELAS = require('../views/section-telas')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.loadPagina = () => {
    window.addEventListener('load', callbackLoadPagina)
  }

  /* ---- Callbacks ---- */

  function callbackLoadPagina () {
    loadTelas()
    loadVisualizacoes()
    loadUrlUsuario()
  }

  /* ---- Aux Functions ---- */

  function loadTelas () {
    HELPER_POVOAR_TELAS().povoar()
  }

  function loadVisualizacoes () {
    REQUEST_PAGINAS().receberPorId()
      .then(pagina => VIEW_VISUALIZACOES().importarDados(pagina))
  }

  function loadUrlUsuario () {
    REQUEST_USUARIO().receberPorId()
      .then(usuario => VIEW_SECTION_TELAS().importarDados(usuario))
  }

  return methods
}

module.exports = Module

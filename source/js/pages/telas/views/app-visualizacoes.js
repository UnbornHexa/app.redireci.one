/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="telas"]')
  const appVisualizacoes = section.querySelector('app-visualizacoes')
  const pVisualizacoes = appVisualizacoes.querySelector('p[name="visualizacoes"]')

  /* ---- Methods ---- */

  methods.importarDados = (dados) => {
    pVisualizacoes.innerText = dados?.visualizacoes || 0
  }

  return methods
}

module.exports = Module

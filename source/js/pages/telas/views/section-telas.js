/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="telas"]')
  const pUrl = section.querySelector('p[name="url"]')

  /* ---- Methods ---- */

  // Data

  methods.importarDados = (dados) => {
    pUrl.innerText = `https://redireci.one/${dados?.usuario}`
  }

  return methods
}

module.exports = Module

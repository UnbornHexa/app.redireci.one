/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_SECTION_SUCESSO = require('./section-sucesso')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()

    EVENT_SECTION_SUCESSO().clickSair()
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_LOGOUT = require('../../../global/helpers/logout')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="sucesso"]')
  const buttonSair = section.querySelector('button[name="sair"]')

  /* ---- Methods ---- */

  methods.clickSair = () => {
    buttonSair.addEventListener('click', callbackClickSair)
  }

  /* ---- Callbacks ---- */

  function callbackClickSair () {
    HELPER_LOGOUT().logout()
  }

  return methods
}

module.exports = Module

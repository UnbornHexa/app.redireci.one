/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="suporte"]')
  const buttonChat = section.querySelector('button[name="chat"]')

  /* ---- Methods ---- */

  methods.clickChat = () => {
    buttonChat.addEventListener('click', callbackClickChat)
  }

  /* ---- Callbacks ---- */

  function callbackClickChat () {
    const chaportLauncher = document.querySelector('.chaport-container .chaport-launcher .chaport-launcher-button')
    chaportLauncher.click()
  }

  return methods
}

module.exports = Module

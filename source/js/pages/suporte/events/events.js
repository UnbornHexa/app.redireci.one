/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_NAV = require('../../../global/elements/nav/events')
const EVENT_SECTION_CONTATO = require('./section-contato')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()
    EVENT_NAV().habilitarTodosOsEventos()

    EVENT_SECTION_CONTATO().clickChat()
  }

  return methods
}

module.exports = Module

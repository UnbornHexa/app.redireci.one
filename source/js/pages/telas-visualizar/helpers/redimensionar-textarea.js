/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.redimensionar = (textarea) => {
    textarea.style.height = 'auto'

    const deslocamento = textarea.offsetHeight - textarea.clientHeight
    const alturaMaxima = 250
    const altura = Math.min(textarea.scrollHeight + deslocamento, alturaMaxima)
    textarea.style.height = `${altura}px`
  }

  return methods
}

module.exports = Module

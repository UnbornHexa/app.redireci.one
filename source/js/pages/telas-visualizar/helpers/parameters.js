/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.receberId = () => {
    const rota = window.location.pathname
    const parametros = rota.split('/')
    return parametros[3]
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const APP_ALERTA = require('../../global/components/app-alerta/init')
const APP_ENVIAR_IMAGEM = require('../../global/components/app-enviar-imagem/init')
const APP_LISTAR_BOTOES_FORMATOS = require('../../global/components/app-listar-botoes-formatos/init')
const APP_LISTAR_FONTES = require('../../global/components/app-listar-fontes/init')
const APP_LISTAR_ICONES = require('../../global/components/app-listar-icones/init')
const APP_LISTAR_ICONES_FORMATOS = require('../../global/components/app-listar-icones-formatos/init')
const APP_LISTAR_FUNDOS = require('../../global/components/app-listar-fundos/init')
const APP_SELECIONR_COR = require('../../global/components/app-selecionar-cor/init')
const HELPER_ASSINATURA = require('./helpers/assinatura')
const HELPER_LOGOUT = require('../../global/helpers/logout')
const EVENTS = require('./events/events')

/* ---- Start ---- */

HELPER_LOGOUT().checkTokenOk()
HELPER_ASSINATURA().detectarAssinante()
EVENTS().habilitarTodosOsEventos()

/* ---- Components ---- */

APP_ALERTA().iniciar()
APP_LISTAR_FONTES().iniciar()
APP_SELECIONR_COR().iniciar()
APP_ENVIAR_IMAGEM().iniciar()
APP_LISTAR_BOTOES_FORMATOS().iniciar()
APP_LISTAR_ICONES().iniciar()
APP_LISTAR_ICONES_FORMATOS().iniciar()
APP_LISTAR_FUNDOS().iniciar()

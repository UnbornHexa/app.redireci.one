/* ---- Requires ---- */

const VIEW_MODAL_ICONE = require('../../views/app-editar/app-modal-icone')
const VIEW_MODAL_ICONE_ENVIO = require('../../views/app-editar/app-modal-icone-envio')
const VIEW_MODAL_ICONE_INICIO = require('../../views/app-editar/app-modal-icone-inicio')
const VIEW_MODAL_ICONE_MODELOS = require('../../views/app-editar/app-modal-icone-modelos')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_icone"]')
  const sectionInicio = appModal.querySelector('section[name="inicio"]')
  const buttonVerModelos = sectionInicio.querySelector('button[name="ver_modelos"]')
  const buttonVerEnvio = sectionInicio.querySelector('button[name="ver_envio"]')
  const buttonVoltar = sectionInicio.querySelector('button[name="cancelar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickVerModelos()
    clickVerEnvio()
    clickVoltar()
  }

  /* ---- Events ---- */

  function clickVerModelos () {
    buttonVerModelos.addEventListener('click', callbackClickVerModelos)
  }

  function clickVerEnvio () {
    buttonVerEnvio.addEventListener('click', callbackClickVerEnvio)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickVerModelos () {
    const sectionModelos = appModal.querySelector('section[name="modelos"]')
    const eventLoad = new window.Event('load')

    VIEW_MODAL_ICONE_INICIO().esconder()
    VIEW_MODAL_ICONE_MODELOS().mostrar()

    sectionModelos.dispatchEvent(eventLoad)
  }

  function callbackClickVerEnvio () {
    VIEW_MODAL_ICONE_INICIO().esconder()
    VIEW_MODAL_ICONE_ENVIO().mostrar()
  }

  function callbackClickVoltar () {
    VIEW_MODAL_ICONE_INICIO().esconder()
    VIEW_MODAL_ICONE().esconder()
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_FUNDOS = require('../../../../global/requests/fundos')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_FUNDO_WALLPAPER = require('../../views/app-editar/app-modal-fundo-wallpaper')
const VIEW_MODAL_FUNDO_INICIO = require('../../views/app-editar/app-modal-fundo-inicio')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionWallpaper = appModal.querySelector('section[name="wallpaper"]')
  const appListarFundos = sectionWallpaper.querySelector('app-listar-fundos')
  const buttonSalvar = sectionWallpaper.querySelector('button[name="salvar"]')
  const buttonVoltar = sectionWallpaper.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickSalvar()
    clickVoltar()
    loadSection()
  }

  /* ---- Events ---- */

  function clickSalvar () {
    buttonSalvar.addEventListener('click', callbackClickSalvar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  function loadSection () {
    sectionWallpaper.addEventListener('load', callbackLoad)
  }

  /* ---- Callbacks ---- */

  function callbackClickSalvar () {
    const dados = VIEW_MODAL_FUNDO_WALLPAPER().exportarDados()

    const camposObrigatorios = VIEW_MODAL_FUNDO_WALLPAPER().verificarCamposObrigatorios()
    if (!camposObrigatorios) return false

    buttonSalvar.setAttribute('disabled', true)
    REQUEST_TELAS().editar(ID_TELA, dados)
      .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      .finally(() => buttonSalvar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_FUNDO_WALLPAPER().esconder()
    VIEW_MODAL_FUNDO_INICIO().mostrar()
  }

  function callbackLoad () {
    REQUEST_FUNDOS().receberTodos()
      .then(fundos => appListarFundos.importarFundos(fundos))

    REQUEST_TELAS().receberPorId(ID_TELA)
      .then(tela => VIEW_MODAL_FUNDO_WALLPAPER().importarDados(tela?.fundo))
  }

  return methods
}

module.exports = Module

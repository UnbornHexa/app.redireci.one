/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIMENSIONAR_TEXTAREA = require('../../helpers/redimensionar-textarea')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_DESCRICAO = require('../../views/app-editar/app-modal-descricao')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_texto"]')
  const textareaDescricao = appModal.querySelector('textarea[name="descricao"]')

  const buttonSalvar = appModal.querySelector('button[name="salvar"]')
  const buttonVoltar = appModal.querySelector('button[name="cancelar"]')

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickSalvar()
    clickVoltar()
    loadModal()
    inputDescricao()
  }

  function clickSalvar () {
    buttonSalvar.addEventListener('click', callbackClickSalvar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  function loadModal () {
    appModal.addEventListener('load', callbackLoadModal)
  }

  function inputDescricao () {
    textareaDescricao.addEventListener('input', callbackInputDescricao)
  }

  /* ---- Callbacks ---- */

  function callbackClickSalvar () {
    const dados = VIEW_MODAL_DESCRICAO().exportarDados()

    const camposObrigatorios = VIEW_MODAL_DESCRICAO().verificarCamposObrigatorios()
    if (!camposObrigatorios) return false

    buttonSalvar.setAttribute('disabled', true)
    REQUEST_TELAS().editar(ID_TELA, dados)
      .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      .finally(() => buttonSalvar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_DESCRICAO().esconder()
  }

  function callbackLoadModal () {
    REQUEST_TELAS().receberPorId(ID_TELA)
      .then(tela => VIEW_MODAL_DESCRICAO().importarDados(tela?.descricao))
  }

  function callbackInputDescricao (evento) {
    HELPER_REDIMENSIONAR_TEXTAREA().redimensionar(evento.target)
  }

  return methods
}

module.exports = Module

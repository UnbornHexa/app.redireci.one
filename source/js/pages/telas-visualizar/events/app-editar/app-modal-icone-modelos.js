/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const REQUEST_ICONES = require('../../../../global/requests/icones')
const VIEW_MODAL_ICONE_INICIO = require('../../views/app-editar/app-modal-icone-inicio')
const VIEW_MODAL_ICONE_MODELOS = require('../../views/app-editar/app-modal-icone-modelos')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_icone"]')
  const sectionModelos = appModal.querySelector('section[name="modelos"]')
  const appListarIcones = sectionModelos.querySelector('app-listar-icones')
  const buttonSalvar = sectionModelos.querySelector('button[name="salvar"]')
  const buttonVoltar = sectionModelos.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    loadSection()
    clickSalvar()
    clickVoltar()
  }

  /* ---- Events ---- */

  function clickSalvar () {
    buttonSalvar.addEventListener('click', callbackClickSalvar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  function loadSection () {
    sectionModelos.addEventListener('load', callbackLoad)
  }

  /* ---- Callbacks ---- */

  function callbackClickSalvar () {
    const dados = VIEW_MODAL_ICONE_MODELOS().exportarDados()

    const camposObrigatorios = VIEW_MODAL_ICONE_MODELOS().verificarCamposObrigatorios()
    if (!camposObrigatorios) return false

    buttonSalvar.setAttribute('disabled', true)
    REQUEST_TELAS().editar(ID_TELA, dados)
      .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      .finally(() => buttonSalvar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_ICONE_MODELOS().esconder()
    VIEW_MODAL_ICONE_INICIO().mostrar()
  }

  function callbackLoad () {
    REQUEST_ICONES().receberTodos()
      .then(icones => appListarIcones.importarIcones(icones))

    REQUEST_TELAS().receberPorId(ID_TELA)
      .then(tela => VIEW_MODAL_ICONE_MODELOS().importarDados(tela?.icone))
  }

  return methods
}

module.exports = Module

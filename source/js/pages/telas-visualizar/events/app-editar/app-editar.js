/* ---- Requires ---- */

const VIEW_MODAL_BOTAO = require('../../views/app-editar/app-modal-botao')
const VIEW_MODAL_DESCRICAO = require('../../views/app-editar/app-modal-descricao')
const VIEW_MODAL_ICONE = require('../../views/app-editar/app-modal-icone')
const VIEW_MODAL_ICONE_INICIO = require('../../views/app-editar/app-modal-icone-inicio')
const VIEW_MODAL_FUNDO = require('../../views/app-editar/app-modal-fundo')
const VIEW_MODAL_FUNDO_INICIO = require('../../views/app-editar/app-modal-fundo-inicio')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appEditar = document.querySelector('app-editar')
  const buttonBotao = appEditar.querySelector('button[name="editar_botao"]')
  const buttonDescricao = appEditar.querySelector('button[name="editar_descricao"]')
  const buttonFundo = appEditar.querySelector('button[name="editar_fundo"]')
  const buttonIcone = appEditar.querySelector('button[name="editar_icone"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickEditarBotao()
    clickEditarDescricao()
    clickEditarFundo()
    clickEditarIcone()
  }

  /* ---- Events ---- */

  function clickEditarBotao () {
    buttonBotao.addEventListener('click', callbackClickEditarBotao)
  }

  function clickEditarDescricao () {
    buttonDescricao.addEventListener('click', callbackClickEditarDescricao)
  }

  function clickEditarFundo () {
    buttonFundo.addEventListener('click', callbackClickEditarFundo)
  }

  function clickEditarIcone () {
    buttonIcone.addEventListener('click', callbackClickEditarIcone)
  }

  /* ---- Callbacks ---- */

  function callbackClickEditarBotao () {
    const appModalEditarBotao = document.querySelector('app-modal[name="editar_botao"]')
    const eventLoad = new window.Event('load')

    VIEW_MODAL_BOTAO().mostrar()
    appModalEditarBotao.dispatchEvent(eventLoad)
  }

  function callbackClickEditarDescricao () {
    const appModalEditarDescricao = document.querySelector('app-modal[name="editar_texto"]')
    const eventLoad = new window.Event('load')

    VIEW_MODAL_DESCRICAO().mostrar()
    appModalEditarDescricao.dispatchEvent(eventLoad)
  }

  function callbackClickEditarFundo () {
    VIEW_MODAL_FUNDO().mostrar()
    VIEW_MODAL_FUNDO_INICIO().mostrar()
  }

  function callbackClickEditarIcone () {
    VIEW_MODAL_ICONE().mostrar()
    VIEW_MODAL_ICONE_INICIO().mostrar()
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_BOTAO = require('../../views/app-editar/app-modal-botao')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_botao"]')
  const buttonVoltar = appModal.querySelector('button[name="cancelar"]')
  const buttonSalvar = appModal.querySelector('button[name="salvar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickSalvar()
    clickVoltar()
    loadModal()
  }

  /* ---- Events ---- */

  function clickSalvar () {
    buttonSalvar.addEventListener('click', callbackClickSalvar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  function loadModal () {
    appModal.addEventListener('load', callbackLoadModal)
  }

  /* ---- Callbacks ---- */

  function callbackClickSalvar () {
    const dados = VIEW_MODAL_BOTAO().exportarDados()

    const camposObrigatorios = VIEW_MODAL_BOTAO().verificarCamposObrigatorios()
    if (!camposObrigatorios) return false

    buttonSalvar.setAttribute('disabled', true)
    REQUEST_TELAS().editar(ID_TELA, dados)
      .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      .finally(() => buttonSalvar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_BOTAO().esconder()
  }

  function callbackLoadModal () {
    REQUEST_TELAS().receberPorId(ID_TELA)
      .then(tela => VIEW_MODAL_BOTAO().importarDados(tela?.botao))
  }

  return methods
}

module.exports = Module

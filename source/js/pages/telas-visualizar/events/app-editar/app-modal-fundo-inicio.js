/* ---- Requires ---- */

const VIEW_MODAL_FUNDO = require('../../views/app-editar/app-modal-fundo')
const VIEW_MODAL_FUNDO_COR = require('../../views/app-editar/app-modal-fundo-cor')
const VIEW_MODAL_FUNDO_DEGRADE = require('../../views/app-editar/app-modal-fundo-degrade')
const VIEW_MODAL_FUNDO_ENVIO = require('../../views/app-editar/app-modal-fundo-envio')
const VIEW_MODAL_FUNDO_INICIO = require('../../views/app-editar/app-modal-fundo-inicio')
const VIEW_MODAL_FUNDO_WALLPAPER = require('../../views/app-editar/app-modal-fundo-wallpaper')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionInicio = appModal.querySelector('section[name="inicio"]')
  const buttonVerCor = sectionInicio.querySelector('button[name="ver_cor"]')
  const buttonVerDegrade = sectionInicio.querySelector('button[name="ver_degrade"]')
  const buttonVerEnvio = sectionInicio.querySelector('button[name="ver_envio"]')
  const buttonVerWallpaper = sectionInicio.querySelector('button[name="ver_wallpaper"]')
  const buttonVoltar = sectionInicio.querySelector('button[name="cancelar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickVoltar()
    clickVerCor()
    clickVerDegrade()
    clickVerEnvio()
    clickVerWallpaper()
  }

  /* ---- Events ---- */

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  function clickVerCor () {
    buttonVerCor.addEventListener('click', callbackClickVerCor)
  }

  function clickVerDegrade () {
    buttonVerDegrade.addEventListener('click', callbackClickVerDegrade)
  }

  function clickVerEnvio () {
    buttonVerEnvio.addEventListener('click', callbackClickVerEnvio)
  }

  function clickVerWallpaper () {
    buttonVerWallpaper.addEventListener('click', callbackClickVerWallpaper)
  }

  /* ---- Callbacks ---- */

  function callbackClickVoltar () {
    VIEW_MODAL_FUNDO_INICIO().esconder()
    VIEW_MODAL_FUNDO().esconder()
  }

  function callbackClickVerCor () {
    const sectionCor = appModal.querySelector('section[name="cor"]')
    const eventLoad = new window.Event('load')

    VIEW_MODAL_FUNDO_INICIO().esconder()
    VIEW_MODAL_FUNDO_COR().mostrar()

    sectionCor.dispatchEvent(eventLoad)
  }

  function callbackClickVerDegrade () {
    const sectionDegrade = appModal.querySelector('section[name="degrade"]')
    const eventLoad = new window.Event('load')

    VIEW_MODAL_FUNDO_INICIO().esconder()
    VIEW_MODAL_FUNDO_DEGRADE().mostrar()

    sectionDegrade.dispatchEvent(eventLoad)
  }

  function callbackClickVerEnvio () {
    VIEW_MODAL_FUNDO_INICIO().esconder()
    VIEW_MODAL_FUNDO_ENVIO().mostrar()
  }

  function callbackClickVerWallpaper () {
    const sectionWallpaper = appModal.querySelector('section[name="wallpaper"]')
    const eventLoad = new window.Event('load')

    VIEW_MODAL_FUNDO_INICIO().esconder()
    VIEW_MODAL_FUNDO_WALLPAPER().mostrar()

    sectionWallpaper.dispatchEvent(eventLoad)
  }

  return methods
}

module.exports = Module

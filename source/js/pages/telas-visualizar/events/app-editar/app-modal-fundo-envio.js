/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_IMGUR = require('../../../../global/requests/imgur')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_FUNDO_ENVIO = require('../../views/app-editar/app-modal-fundo-envio')
const VIEW_MODAL_FUNDO_INICIO = require('../../views/app-editar/app-modal-fundo-inicio')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionCor = appModal.querySelector('section[name="envio"]')
  const buttonSalvar = sectionCor.querySelector('button[name="salvar"]')
  const buttonVoltar = sectionCor.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickSalvar()
    clickVoltar()
  }

  /* ---- Events ---- */

  function clickSalvar () {
    buttonSalvar.addEventListener('click', callbackClickSalvar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickSalvar () {
    const formData = VIEW_MODAL_FUNDO_ENVIO().exportarFormData()

    const camposObrigatorios = VIEW_MODAL_FUNDO_ENVIO().verificarCamposObrigatorios()
    if (!camposObrigatorios) return false

    buttonSalvar.setAttribute('disabled', true)
    REQUEST_IMGUR().uploadFundo(formData)
      .then(tela => {
        const dados = VIEW_MODAL_FUNDO_ENVIO().exportarDados(tela)
        REQUEST_TELAS().editar(ID_TELA, dados)
          .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      })
      .finally(() => buttonSalvar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_FUNDO_ENVIO().esconder()
    VIEW_MODAL_FUNDO_INICIO().mostrar()
  }

  return methods
}

module.exports = Module

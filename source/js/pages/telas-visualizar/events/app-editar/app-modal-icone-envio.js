/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_IMGUR = require('../../../../global/requests/imgur')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_ICONE_ENVIO = require('../../views/app-editar/app-modal-icone-envio')
const VIEW_MODAL_ICONE_INICIO = require('../../views/app-editar/app-modal-icone-inicio')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_icone"]')
  const sectionEnvio = appModal.querySelector('section[name="envio"]')
  const buttonEnviar = sectionEnvio.querySelector('button[name="enviar"]')
  const buttonVoltar = sectionEnvio.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickEnviar()
    clickVoltar()
  }

  /* ---- Events ---- */

  function clickEnviar () {
    buttonEnviar.addEventListener('click', callbackClickEnviar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickEnviar () {
    const formData = VIEW_MODAL_ICONE_ENVIO().exportarFormData()

    const camposObrigatorios = VIEW_MODAL_ICONE_ENVIO().verificarCamposObrigatorios()
    if (!camposObrigatorios) return false

    buttonEnviar.setAttribute('disabled', true)
    REQUEST_IMGUR().uploadIcone(formData)
      .then(tela => {
        const dados = VIEW_MODAL_ICONE_ENVIO().exportarDados(tela)
        REQUEST_TELAS().editar(ID_TELA, dados)
          .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      })
      .finally(() => buttonEnviar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_ICONE_ENVIO().esconder()
    VIEW_MODAL_ICONE_INICIO().mostrar()
  }

  return methods
}

module.exports = Module

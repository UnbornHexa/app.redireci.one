/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_FUNDO_COR = require('../../views/app-editar/app-modal-fundo-cor')
const VIEW_MODAL_FUNDO_INICIO = require('../../views/app-editar/app-modal-fundo-inicio')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionCor = appModal.querySelector('section[name="cor"]')
  const buttonSalvar = sectionCor.querySelector('button[name="salvar"]')
  const buttonVoltar = sectionCor.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickSalvar()
    clickVoltar()
    loadSection()
  }

  /* ---- Events ---- */

  function clickSalvar () {
    buttonSalvar.addEventListener('click', callbackClickSalvar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  function loadSection () {
    sectionCor.addEventListener('load', callbackLoad)
  }

  /* ---- Callbacks ---- */

  function callbackClickSalvar () {
    const dados = VIEW_MODAL_FUNDO_COR().exportarDados()

    const camposObrigatorios = VIEW_MODAL_FUNDO_COR().verificarCamposObrigatorios()
    if (!camposObrigatorios) return false

    buttonSalvar.setAttribute('disabled', true)
    REQUEST_TELAS().editar(ID_TELA, dados)
      .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      .finally(() => buttonSalvar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_FUNDO_COR().esconder()
    VIEW_MODAL_FUNDO_INICIO().mostrar()
  }

  function callbackLoad () {
    REQUEST_TELAS().receberPorId(ID_TELA)
      .then(tela => VIEW_MODAL_FUNDO_COR().importarDados(tela?.fundo))
  }

  return methods
}

module.exports = Module

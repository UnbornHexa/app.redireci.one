/* ---- Requires ---- */

const APP_EDITAR = require('./app-editar/app-editar')
const APP_MODAL_BOTAO = require('./app-editar/app-modal-botao')
const APP_MODAL_DELETAR = require('./app-opcoes/app-modal-deletar')
const APP_MODAL_DESCRICAO = require('./app-editar/app-modal-descricao')
const APP_MODAL_NOME = require('./app-opcoes/app-modal-nome')

const APP_MODAL_ICONE_ENVIO = require('./app-editar/app-modal-icone-envio')
const APP_MODAL_ICONE_INICIO = require('./app-editar/app-modal-icone-inicio')
const APP_MODAL_ICONE_MODELOS = require('./app-editar/app-modal-icone-modelos')

const APP_MODAL_FUNDO_COR = require('./app-editar/app-modal-fundo-cor')
const APP_MODAL_FUNDO_DEGRADE = require('./app-editar/app-modal-fundo-degrade')
const APP_MODAL_FUNDO_ENVIO = require('./app-editar/app-modal-fundo-envio')
const APP_MODAL_FUNDO_INICIO = require('./app-editar/app-modal-fundo-inicio')
const APP_MODAL_FUNDO_WALLPAPER = require('./app-editar/app-modal-fundo-wallpaper')

const APP_OPCOES = require('./app-opcoes/app-opcoes')
const EVENTS = require('../../../global/events/events')
const NAV = require('../../../global/elements/nav/events')
const SECTION_PREVIEW = require('./section-preview')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENTS().habilitarTodosOsEventosGlobais()
    NAV().habilitarTodosOsEventos()

    /* ---- */

    SECTION_PREVIEW().habilitarEventos()

    APP_OPCOES().habilitarEventos()
    APP_MODAL_DELETAR().habilitarEventos()
    APP_MODAL_NOME().habilitarEventos()

    APP_EDITAR().habilitarEventos()

    APP_MODAL_BOTAO().habilitarEventos()

    APP_MODAL_DESCRICAO().habilitarEventos()

    APP_MODAL_ICONE_INICIO().habilitarEventos()
    APP_MODAL_ICONE_MODELOS().habilitarEventos()
    APP_MODAL_ICONE_ENVIO().habilitarEventos()

    APP_MODAL_FUNDO_COR().habilitarEventos()
    APP_MODAL_FUNDO_DEGRADE().habilitarEventos()
    APP_MODAL_FUNDO_ENVIO().habilitarEventos()
    APP_MODAL_FUNDO_INICIO().habilitarEventos()
    APP_MODAL_FUNDO_WALLPAPER().habilitarEventos()
  }

  return methods
}

module.exports = Module

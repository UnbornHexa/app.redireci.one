/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const VIEW_MODAL_DELETAR = require('../../views/app-opcoes/app-modal-deletar')
const VIEW_MODAL_NOME = require('../../views/app-opcoes/app-modal-nome')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appOpcoes = document.querySelector('app-opcoes')
  const buttonVoltar = appOpcoes.querySelector('button[name="voltar"]')
  const buttonDeletar = appOpcoes.querySelector('button[name="deletar"]')
  const buttonNome = appOpcoes.querySelector('button[name="nome"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickDeletar()
    clickNome()
    clickVoltar()
  }

  function clickDeletar () {
    buttonDeletar.addEventListener('click', callbackClickDeletar)
  }

  function clickNome () {
    buttonNome.addEventListener('click', callbackClickNome)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickDeletar () {
    VIEW_MODAL_DELETAR().mostrar()
  }

  function callbackClickNome () {
    VIEW_MODAL_NOME().mostrar()
  }

  function callbackClickVoltar () {
    HELPER_REDIRECT().toPage('telas')
  }

  return methods
}

module.exports = Module

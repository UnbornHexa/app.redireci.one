/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_NOME = require('../../views/app-opcoes/app-modal-nome')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_nome"]')
  const buttonVoltar = appModal.querySelector('button[name="voltar"]')
  const buttonSalvar = appModal.querySelector('button[name="salvar"]')

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickSalvar()
    clickVoltar()
  }

  function clickSalvar () {
    buttonSalvar.addEventListener('click', callbackSalvar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackSalvar () {
    const dados = VIEW_MODAL_NOME().exportarDados()

    const camposObrigatoriosOk = VIEW_MODAL_NOME().verificarCamposObrigatorios()
    if (!camposObrigatoriosOk) return

    buttonSalvar.setAttribute('disabled', true)
    REQUEST_TELAS().editar(ID_TELA, dados)
      .then(() => HELPER_REDIRECT().toPage(`telas/visualizar/${ID_TELA}`))
      .finally(() => buttonSalvar.removeAttribute('disabled'))
  }

  function callbackVoltar () {
    VIEW_MODAL_NOME().esconder()
  }

  return methods
}

module.exports = Module

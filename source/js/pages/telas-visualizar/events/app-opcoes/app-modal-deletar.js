/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../../helpers/parameters')
const HELPER_REDIRECT = require('../../../../global/helpers/redirect')
const REQUEST_TELAS = require('../../../../global/requests/telas')
const VIEW_MODAL_DELETAR = require('../../views/app-opcoes/app-modal-deletar')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="deletar_tela"]')
  const buttonDeletar = appModal.querySelector('button[name="deletar"]')
  const buttonVoltar = appModal.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    clickDeletar()
    clickVoltar()
  }

  function clickDeletar () {
    buttonDeletar.addEventListener('click', callbackClickDeletar)
  }

  function clickVoltar () {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickDeletar () {
    buttonDeletar.setAttribute('disabled', true)
    REQUEST_TELAS().deletar(ID_TELA)
      .then(() => HELPER_REDIRECT().toPage('telas'))
      .finally(() => buttonDeletar.removeAttribute('disabled'))
  }

  function callbackClickVoltar () {
    VIEW_MODAL_DELETAR().esconder()
  }

  return methods
}

module.exports = Module

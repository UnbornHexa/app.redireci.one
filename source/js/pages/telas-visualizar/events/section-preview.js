/* ---- Requires ---- */

const HELPER_PARAMETERS = require('../helpers/parameters')
const HELPER_REDIRECT = require('../../../global/helpers/redirect')
const REQUEST_TELAS = require('../../../global/requests/telas')
const VIEW_SECTION_PREVIEW = require('../views/section-preview')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const ID_TELA = HELPER_PARAMETERS().receberId()

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="preview"]')
  const h2Nome = document.querySelector('h2[name="nome_tela"]')
  const pNome = document.querySelector('p[name="nome_atual"]')
  const buttonBotao = section.querySelector('button[name="botao"]')

  /* ---- Methods ---- */

  methods.habilitarEventos = () => {
    loadPreview()
    clickBotao()
  }

  function loadPreview () {
    window.addEventListener('load', callbackLoadPreview)
  }

  function clickBotao () {
    buttonBotao.addEventListener('click', callbackClickBotao)
  }

  /* ---- Callbacks ---- */

  function callbackLoadPreview () {
    REQUEST_TELAS().receberPorId(ID_TELA)
      .then(tela => {
        document.title = tela?.nome
        h2Nome.innerText = tela?.nome
        pNome.innerText = tela?.nome

        VIEW_SECTION_PREVIEW().carregarBotao(tela?.botao)
        VIEW_SECTION_PREVIEW().carregarDescricao(tela?.descricao)
        VIEW_SECTION_PREVIEW().carregarFundo(tela?.fundo)
        VIEW_SECTION_PREVIEW().carregarIcone(tela?.icone)
      })
  }

  function callbackClickBotao () {
    const url = buttonBotao.getAttribute('data-url')
    HELPER_REDIRECT().toExternalPage(url)
  }

  return methods
}

module.exports = Module

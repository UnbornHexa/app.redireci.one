/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="preview"]')
  const buttonBotao = section.querySelector('button[name="botao"]')
  const pDescricao = section.querySelector('p[name="descricao"]')
  const imgIcone = section.querySelector('img[name="icone"]')

  /* ---- Methods ---- */

  methods.carregarBotao = (botao) => {
    buttonBotao.style.color = botao?.corTexto
    buttonBotao.style.backgroundColor = botao?.corFundo

    const classeFormato = `formato_botao_${botao?.formato}`
    buttonBotao.classList.add(classeFormato)

    const classFonte = `fonte_${botao?.fonte}`
    buttonBotao.classList.add(classFonte)

    buttonBotao.innerText = botao?.texto
    buttonBotao.setAttribute('data-url', botao?.url)
  }

  methods.carregarDescricao = (descricao) => {
    const classFonte = `fonte_${descricao?.fonte}`
    pDescricao.classList.add(classFonte)

    pDescricao.style.color = descricao?.cor
    pDescricao.innerText = descricao?.texto
  }

  methods.carregarIcone = (icone) => {
    const classeFormato = `formato_icone_${icone?.formato}`
    imgIcone.classList.add(classeFormato)

    imgIcone.src = icone?.imagem
  }

  // Fundo

  methods.carregarFundo = (fundo) => {
    const fundoTipoSelecionado = fundo?.selecionado
    if (fundoTipoSelecionado === 'cor') carregarFundoCor(fundo)
    else if (fundoTipoSelecionado === 'gradiente') carregarFundoGradiente(fundo)
    else if (fundoTipoSelecionado === 'imagem') carregarFundoImagem(fundo)
  }

  /* ---- Aux Functions ---- */

  function carregarFundoCor (fundo) {
    section.style.backgroundColor = fundo?.cor
  }

  function carregarFundoGradiente (fundo) {
    const cor1 = fundo.gradiente?.cor1
    const cor2 = fundo.gradiente?.cor2
    section.style.backgroundImage = `linear-gradient(${cor1}, ${cor2})`
  }

  function carregarFundoImagem (fundo) {
    section.style.backgroundImage = `url(${fundo?.imagem})`
  }

  return methods
}

module.exports = Module

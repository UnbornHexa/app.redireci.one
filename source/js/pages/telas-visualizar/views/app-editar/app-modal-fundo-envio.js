/* ---- Requires ---- */

const VIEW_PREVIEW_MODAL_FUNDO_ENVIO = require('../previews/app-modal-fundo-envio')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SECTION_OCULTAR = 'ocultar'
  const CLASSE_SECTION_MOSTRAR = 'mostrar'

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionEnvio = appModal.querySelector('section[name="envio"]')
  const appEnviarImagem = sectionEnvio.querySelector('app-enviar-imagem')
  const divFundo = sectionEnvio.querySelector('div[name="fundo"]')
  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  // Section

  methods.mostrar = () => {
    sectionEnvio.classList.remove(CLASSE_SECTION_OCULTAR)
    sectionEnvio.classList.add(CLASSE_SECTION_MOSTRAR)
    VIEW_PREVIEW_MODAL_FUNDO_ENVIO().iniciarTimer()
  }

  methods.esconder = () => {
    methods.limparCampos()
    sectionEnvio.classList.add(CLASSE_SECTION_OCULTAR)
    sectionEnvio.classList.remove(CLASSE_SECTION_MOSTRAR)
    VIEW_PREVIEW_MODAL_FUNDO_ENVIO().pausarTimer()
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (appEnviarImagem.vazio()) appAlerta.alertar('Carregue um wallpaper do seu dispositivo.')
    else return true

    return false
  }

  methods.limparCampos = () => {
    appEnviarImagem.limpar()
    divFundo.style.backgroundImage = ''
  }

  // Data

  methods.exportarFormData = () => {
    return appEnviarImagem.exportarFormData()
  }

  methods.exportarDados = (urlImagem) => {
    const tela = {
      'fundo.selecionado': 'imagem',
      'fundo.imagem': urlImagem
    }

    return tela
  }

  return methods
}

module.exports = Module

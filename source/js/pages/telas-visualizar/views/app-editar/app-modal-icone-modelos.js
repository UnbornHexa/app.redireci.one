/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SECTION_OCULTAR = 'ocultar'
  const CLASSE_SECTION_MOSTRAR = 'mostrar'

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_icone"]')
  const sectionModelos = appModal.querySelector('section[name="modelos"]')
  const appListarIcones = sectionModelos.querySelector('app-listar-icones')
  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  // Section

  methods.mostrar = () => {
    sectionModelos.classList.remove(CLASSE_SECTION_OCULTAR)
    sectionModelos.classList.add(CLASSE_SECTION_MOSTRAR)
  }

  methods.esconder = () => {
    sectionModelos.classList.add(CLASSE_SECTION_OCULTAR)
    sectionModelos.classList.remove(CLASSE_SECTION_MOSTRAR)
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (appListarIcones.vazio()) appAlerta.alertar('Selecione um ícone.')
    else return true

    return false
  }

  // Data

  methods.importarDados = (dados) => {
    appListarIcones.importarIconeSelecionado(dados?.imagem)
  }

  methods.exportarDados = () => {
    const imagem = appListarIcones.exportarIconeSelecionado()
    const tela = {
      'icone.formato': 'quadrado',
      'icone.imagem': imagem
    }

    return tela
  }

  return methods
}

module.exports = Module

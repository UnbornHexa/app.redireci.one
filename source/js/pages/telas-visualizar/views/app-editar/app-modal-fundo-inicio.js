/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SECTION_OCULTAR = 'ocultar'
  const CLASSE_SECTION_MOSTRAR = 'mostrar'

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionInicio = appModal.querySelector('section[name="inicio"]')

  /* ---- Methods ---- */

  // Section

  methods.mostrar = () => {
    sectionInicio.classList.remove(CLASSE_SECTION_OCULTAR)
    sectionInicio.classList.add(CLASSE_SECTION_MOSTRAR)
  }

  methods.esconder = () => {
    sectionInicio.classList.add(CLASSE_SECTION_OCULTAR)
    sectionInicio.classList.remove(CLASSE_SECTION_MOSTRAR)
  }

  return methods
}

module.exports = Module

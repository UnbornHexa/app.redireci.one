/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SECTION_OCULTAR = 'ocultar'
  const CLASSE_SECTION_MOSTRAR = 'mostrar'

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionDegrade = appModal.querySelector('section[name="degrade"]')
  const appSelecionarCor1 = sectionDegrade.querySelector('app-selecionar-cor[name="cor1"]')
  const appSelecionarCor2 = sectionDegrade.querySelector('app-selecionar-cor[name="cor2"]')
  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  // Section

  methods.mostrar = () => {
    sectionDegrade.classList.remove(CLASSE_SECTION_OCULTAR)
    sectionDegrade.classList.add(CLASSE_SECTION_MOSTRAR)
  }

  methods.esconder = () => {
    sectionDegrade.classList.add(CLASSE_SECTION_OCULTAR)
    sectionDegrade.classList.remove(CLASSE_SECTION_MOSTRAR)
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (appSelecionarCor1.vazio()) appAlerta.alertar('Selecione a primeira cor do gradiente.')
    else if (appSelecionarCor2.vazio()) appAlerta.alertar('Selecione a segunda cor do gradiente.')
    else return true

    return false
  }

  // Data

  methods.importarDados = (dados) => {
    if (dados?.selecionado !== 'gradiente') return

    appSelecionarCor1.importarCor(dados?.gradiente?.cor1)
    appSelecionarCor2.importarCor(dados?.gradiente?.cor2)
  }

  methods.exportarDados = () => {
    const tela = {
      'fundo.selecionado': 'gradiente',
      'fundo.gradiente.cor1': appSelecionarCor1.exportarCor(),
      'fundo.gradiente.cor2': appSelecionarCor2.exportarCor()
    }

    return tela
  }

  return methods
}

module.exports = Module

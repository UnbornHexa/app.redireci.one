/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SECTION_OCULTAR = 'ocultar'
  const CLASSE_SECTION_MOSTRAR = 'mostrar'

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionCor = appModal.querySelector('section[name="cor"]')
  const appSelecionarCor = sectionCor.querySelector('app-selecionar-cor')
  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  // Section

  methods.mostrar = () => {
    sectionCor.classList.remove(CLASSE_SECTION_OCULTAR)
    sectionCor.classList.add(CLASSE_SECTION_MOSTRAR)
  }

  methods.esconder = () => {
    sectionCor.classList.add(CLASSE_SECTION_OCULTAR)
    sectionCor.classList.remove(CLASSE_SECTION_MOSTRAR)
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (appSelecionarCor.vazio()) appAlerta.alertar('Selecione uma cor para o fundo.')
    else return true

    return false
  }

  // Data

  methods.importarDados = (dados) => {
    if (dados?.selecionado !== 'cor') return
    appSelecionarCor.importarCor(dados?.cor)
  }

  methods.exportarDados = () => {
    const tela = {
      'fundo.selecionado': 'cor',
      'fundo.cor': appSelecionarCor.exportarCor()
    }
    return tela
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const HELPER_MODAL = require('../../../../global/elements/app-modal/helper-modal')
const HELPER_REDIMENSIONAR_TEXTAREA = require('../../helpers/redimensionar-textarea')
const VIEW_PREVIEW_MODAL_DESCRICAO = require('../previews/app-modal-descricao')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_texto"]')
  const appAlerta = document.querySelector('app-alerta')

  const appSelecionarCor = appModal.querySelector('app-selecionar-cor')
  const appListarFontes = appModal.querySelector('app-listar-fontes')
  const textareaDescricao = appModal.querySelector('textarea[name="descricao"]')

  /* ---- Methods ---- */

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL().abrirModal(appModal)
    VIEW_PREVIEW_MODAL_DESCRICAO().iniciarTimer()
  }

  methods.esconder = () => {
    HELPER_MODAL().fecharModal(appModal)
    VIEW_PREVIEW_MODAL_DESCRICAO().pausarTimer()
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (!textareaDescricao.value) appAlerta.alertar('Digite a descrição.')
    else if (appListarFontes.vazio()) appAlerta.alertar('Selecione uma fonte para a descrição.')
    else if (appSelecionarCor.vazio()) appAlerta.alertar('Seleciona uma cor para a descrição.')
    else return true

    return false
  }

  // Data

  methods.importarDados = (dados) => {
    textareaDescricao.value = dados?.texto
    HELPER_REDIMENSIONAR_TEXTAREA().redimensionar(textareaDescricao)

    appListarFontes.importarFonteSelecionada(dados?.fonte)
    appSelecionarCor.importarCor(dados?.cor)
  }

  methods.exportarDados = () => {
    const tela = {
      'descricao.texto': textareaDescricao.value,
      'descricao.fonte': appListarFontes.exportarFonteSelecionada(),
      'descricao.cor': appSelecionarCor.exportarCor()
    }
    return tela
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const VIEW_PREVIEW_MODAL_ICONE_ENVIO = require('../previews/app-modal-icone-envio')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SECTION_OCULTAR = 'ocultar'
  const CLASSE_SECTION_MOSTRAR = 'mostrar'

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_icone"]')
  const sectionEnvio = appModal.querySelector('section[name="envio"]')
  const appListarIconesFormatos = sectionEnvio.querySelector('app-listar-icones-formatos')
  const appEnviarImagem = sectionEnvio.querySelector('app-enviar-imagem')
  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  // Section

  methods.mostrar = () => {
    sectionEnvio.classList.remove(CLASSE_SECTION_OCULTAR)
    sectionEnvio.classList.add(CLASSE_SECTION_MOSTRAR)
    VIEW_PREVIEW_MODAL_ICONE_ENVIO().iniciarTimer()
  }

  methods.esconder = () => {
    methods.limparCampos()
    sectionEnvio.classList.add(CLASSE_SECTION_OCULTAR)
    sectionEnvio.classList.remove(CLASSE_SECTION_MOSTRAR)
    VIEW_PREVIEW_MODAL_ICONE_ENVIO().pausarTimer()
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (appEnviarImagem.vazio()) appAlerta.alertar('Carregue um ícone do seu dispositivo.')
    else if (appListarIconesFormatos.vazio()) appAlerta.alertar('Selecione um formato para o ícone.')
    else return true

    return false
  }

  methods.limparCampos = () => {
    appEnviarImagem.limpar()
    appListarIconesFormatos.limpar()
  }

  // Data

  methods.exportarFormData = () => {
    return appEnviarImagem.exportarFormData()
  }

  methods.exportarDados = (urlImagem) => {
    const tela = {
      'icone.formato': appListarIconesFormatos.exportarFormatoSelecionado(),
      'icone.imagem': urlImagem
    }

    return tela
  }

  return methods
}

module.exports = Module

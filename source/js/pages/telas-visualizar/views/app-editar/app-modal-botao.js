/* ---- Requires ---- */

const HELPER_MODAL = require('../../../../global/elements/app-modal/helper-modal')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_botao"]')
  const appAlerta = document.querySelector('app-alerta')

  const appListarBotoesFormatos = appModal.querySelector('app-listar-botoes-formatos')
  const appListarFontes = appModal.querySelector('app-listar-fontes')
  const appSelecionarFundo = appModal.querySelector('app-selecionar-cor[name="cor_fundo"]')
  const appSelecionarTexto = appModal.querySelector('app-selecionar-cor[name="cor_texto"]')

  const inputTexto = appModal.querySelector('input[name="texto"]')
  const inputUrl = appModal.querySelector('input[name="url"]')

  /* ---- Methods ---- */

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL().abrirModal(appModal)
  }

  methods.esconder = () => {
    HELPER_MODAL().fecharModal(appModal)
  }

  // Data

  methods.importarDados = (dados) => {
    inputTexto.value = dados?.texto
    inputUrl.value = dados?.url
    appListarBotoesFormatos.importarFormatoSelecionado(dados?.formato)
    appListarFontes.importarFonteSelecionada(dados?.fonte)
    appSelecionarFundo.importarCor(dados?.corFundo)
    appSelecionarTexto.importarCor(dados?.corTexto)
  }

  methods.exportarDados = () => {
    const tela = {
      'botao.texto': inputTexto.value,
      'botao.url': sanitizarURL(inputUrl.value),
      'botao.formato': appListarBotoesFormatos.exportarFormatoSelecionado(),
      'botao.fonte': appListarFontes.exportarFonteSelecionada(),
      'botao.corFundo': appSelecionarFundo.exportarCor(),
      'botao.corTexto': appSelecionarTexto.exportarCor()
    }

    return tela
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (!inputTexto.value) appAlerta.alertar('Escreva um texto para o botão.')
    else if (appListarBotoesFormatos.vazio()) appAlerta.alertar('Selecione um formato para o botão.')
    else if (appListarFontes.vazio()) appAlerta.alertar('Selecione uma fonte para o botão.')
    else if (appSelecionarTexto.vazio()) appAlerta.alertar('Selecione a cor do texto do botão.')
    else if (appSelecionarFundo.vazio()) appAlerta.alertar('Selecione a cor do fundo do botão.')
    else return true

    return false
  }

  /* ---- Aux Functions ---- */

  function sanitizarURL (url) {
    if (!url) return ''

    const regex = /https?:\/\//gi
    const urlSanitizada = url.replace(regex, '')
    return `https://${urlSanitizada}`
  }

  return methods
}

module.exports = Module

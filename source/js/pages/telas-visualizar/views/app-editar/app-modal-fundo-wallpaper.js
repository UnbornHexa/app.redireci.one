/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const CLASSE_SECTION_OCULTAR = 'ocultar'
  const CLASSE_SECTION_MOSTRAR = 'mostrar'

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionWallpaper = appModal.querySelector('section[name="wallpaper"]')
  const appListarFundos = sectionWallpaper.querySelector('app-listar-fundos')
  const appAlerta = document.querySelector('app-alerta')

  /* ---- Methods ---- */

  // Section

  methods.mostrar = () => {
    sectionWallpaper.classList.remove(CLASSE_SECTION_OCULTAR)
    sectionWallpaper.classList.add(CLASSE_SECTION_MOSTRAR)
  }

  methods.esconder = () => {
    sectionWallpaper.classList.add(CLASSE_SECTION_OCULTAR)
    sectionWallpaper.classList.remove(CLASSE_SECTION_MOSTRAR)
  }

  // Form

  methods.verificarCamposObrigatorios = () => {
    if (appListarFundos.vazio()) appAlerta.alertar('Selecione um wallpaper.')
    else return true

    return false
  }

  // Data

  methods.importarDados = (dados) => {
    appListarFundos.importarFundoSelecionado(dados?.imagem)

    const tipoFundoSelecionado = dados?.selecionado
    if (tipoFundoSelecionado !== 'imagem') return

    appListarFundos.importarFundoSelecionado(dados?.imagem)
  }

  methods.exportarDados = () => {
    const tela = {
      'fundo.selecionado': 'imagem',
      'fundo.imagem': appListarFundos.exportarFundoSelecionado()
    }

    return tela
  }

  return methods
}

module.exports = Module

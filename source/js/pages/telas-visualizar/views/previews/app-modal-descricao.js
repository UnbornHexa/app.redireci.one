/* ---- Singleton Variables ---- */

let timer = null

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_texto"]')
  const appSelecionarCor = appModal.querySelector('app-selecionar-cor')
  const appListarFontes = appModal.querySelector('app-listar-fontes')
  const textareaDescricao = appModal.querySelector('textarea[name="descricao"]')

  /* ---- Methods ---- */

  methods.iniciarTimer = () => {
    timer = setInterval(atualizarPreview, 200)
  }

  methods.pausarTimer = () => {
    clearInterval(timer)
  }

  /* ---- View ---- */

  function atualizarPreview () {
    const fonte = appListarFontes.exportarFonteSelecionada()
    const cor = appSelecionarCor.exportarCor()

    textareaDescricao.style.fontFamily = fonte
    textareaDescricao.style.color = cor
  }

  return methods
}

module.exports = Module

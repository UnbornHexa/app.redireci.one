/* ---- Singleton Variables ---- */

let timer = null

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_fundo"]')
  const sectionEnvio = appModal.querySelector('section[name="envio"]')
  const divFundo = sectionEnvio.querySelector('div[name="fundo"]')
  const appEnviarImagem = sectionEnvio.querySelector('app-enviar-imagem')

  /* ---- Methods ---- */

  methods.iniciarTimer = () => {
    timer = setInterval(atualizarPreview, 1000)
  }

  methods.pausarTimer = () => {
    clearInterval(timer)
  }

  /* ---- View ---- */

  function atualizarPreview () {
    if (appEnviarImagem.vazio()) return

    appEnviarImagem.exportarImagem()
      .then(imagem => { divFundo.style.backgroundImage = `url(${imagem})` })
  }

  return methods
}

module.exports = Module

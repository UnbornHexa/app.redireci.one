/* ---- Singleton Variables ---- */

let timer = null

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_icone"]')
  const sectionEnvio = appModal.querySelector('section[name="envio"]')
  const appListarIconesFormatos = sectionEnvio.querySelector('app-listar-icones-formatos')
  const appEnviarImagem = sectionEnvio.querySelector('app-enviar-imagem')

  /* ---- Methods ---- */

  methods.iniciarTimer = () => {
    timer = setInterval(atualizarPreview, 1000)
  }

  methods.pausarTimer = () => {
    clearInterval(timer)
  }

  /* ---- View ---- */

  function atualizarPreview () {
    if (appEnviarImagem.vazio()) return

    appEnviarImagem.exportarImagem()
      .then(imagem => appListarIconesFormatos.definirImagemFundo(imagem))
  }

  return methods
}

module.exports = Module

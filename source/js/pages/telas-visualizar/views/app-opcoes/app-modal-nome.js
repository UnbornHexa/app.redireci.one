/* ---- Requires ---- */

const HELPER_MODAL = require('../../../../global/elements/app-modal/helper-modal')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const appModal = document.querySelector('app-modal[name="editar_nome"]')
  const appAlerta = document.querySelector('app-alerta')
  const inputNome = appModal.querySelector('input[name="nome"]')

  /* ---- Methods ---- */

  // Modal

  methods.mostrar = () => {
    HELPER_MODAL().abrirModal(appModal)
  }

  methods.esconder = () => {
    HELPER_MODAL().fecharModal(appModal)
  }

  // Form

  methods.limparCampos = () => {
    inputNome.value = ''
  }

  methods.verificarCamposObrigatorios = () => {
    if (!inputNome.value) appAlerta.alertar('Digite um nome para a Tela')
    else return true

    return false
  }

  // Data

  methods.exportarDados = () => {
    const tela = { nome: inputNome.value }
    return tela
  }

  return methods
}

module.exports = Module

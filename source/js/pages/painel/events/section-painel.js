/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../../global/helpers/redirect')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const divLinks = document.querySelector('div[name="links"]')

  /* ---- Methods ---- */

  methods.clickLinks = () => {
    divLinks.addEventListener('click', callbackClickLinks)
  }

  /* ---- Callbacks ---- */

  function callbackClickLinks () {
    HELPER_REDIRECT().toPage('telas')
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const REQUEST_USUARIO = require('../../../global/requests/usuarios')
const VIEW_SECTION_PAINEL = require('../views/section-painel')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.loadNomeUsuario = () => {
    window.addEventListener('load', callbackLoadNomeUsuario)
  }

  /* ---- Callbacks ---- */

  function callbackLoadNomeUsuario () {
    REQUEST_USUARIO().receberPorId()
      .then(usuario => VIEW_SECTION_PAINEL().importarDados(usuario))
  }

  return methods
}

module.exports = Module

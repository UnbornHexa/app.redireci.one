/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_NAV = require('../../../global/elements/nav/events')
const EVENT_SECTION_PAINEL = require('./section-painel')
const EVENT_WINDOW = require('./window')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()
    EVENT_NAV().habilitarTodosOsEventos()

    EVENT_WINDOW().loadNomeUsuario()
    EVENT_SECTION_PAINEL().clickLinks()
  }

  return methods
}

module.exports = Module

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const spanNome = document.querySelector('span[name="nome"]')

  /* ---- Methods ---- */

  methods.importarDados = (dados) => {
    const nome = dados?.nome || ''

    const primeiroNome = (nome.split(' ').length > 0)
      ? nome.split(' ')[0]
      : nome

    spanNome.innerText = `${primeiroNome}!`
  }

  return methods
}

module.exports = Module

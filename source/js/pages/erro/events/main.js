/* ---- Requires ---- */

const HELPER_REDIRECT = require('../../../global/helpers/redirect')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const buttonVoltar = document.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.clickVoltar = () => {
    buttonVoltar.addEventListener('click', callbackClickVoltar)
  }

  /* ---- Callbacks ---- */

  function callbackClickVoltar () {
    HELPER_REDIRECT().toPage('')
  }

  return methods
}

module.exports = Module

/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_MAIN = require('./main')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()
    EVENT_MAIN().clickVoltar()
  }

  return methods
}

module.exports = Module

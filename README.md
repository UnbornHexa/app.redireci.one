1. Baixando o Projeto
```
git clone https://gitlab.com/redirecione/frontend/app.git
```

2. Instalando as Dependências
```
yarn install
```

3. Executando a Aplicação

3.1. Desenvolvimento
```
yarn run dev #(terminal 1)
yarn run watch #(terminal 2)
```

3.2. Produção
```
yarn run prod
```

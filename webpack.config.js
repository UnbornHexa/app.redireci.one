/* Requires */

const webpack = require('webpack')
const path = require('path')
const modoDev = process.env.NODE_ENV !== 'production'
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

/* Modulo */

module.exports = {
  mode: modoDev ? 'development' : 'production',
  stats: {
    assets: false,
    children: false,
    chunks: false,
    modules: false,
    entrypoints: false
  },

  /* Entradas */
  entry: {
    assinatura: path.resolve(__dirname, './webpack/assinatura.js'),
    'assinatura-sucesso': path.resolve(__dirname, './webpack/assinatura-sucesso.js'),
    entrar: path.resolve(__dirname, './webpack/entrar.js'),
    erro: path.resolve(__dirname, './webpack/erro.js'),
    telas: path.resolve(__dirname, './webpack/telas.js'),
    'telas-visualizar': path.resolve(__dirname, './webpack/telas-visualizar.js'),
    painel: path.resolve(__dirname, './webpack/painel.js'),
    perfil: path.resolve(__dirname, './webpack/perfil.js'),
    registrar: path.resolve(__dirname, './webpack/registrar.js'),
    suporte: path.resolve(__dirname, './webpack/suporte.js')
  },

  /* Saida */
  output: {
    filename: 'assets/js/[name]-[contenthash:8].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },

  /* Otimizacoes */
  optimization: {
    splitChunks: {
      chunks: 'all'
    },
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: { ecma: 6 }
      }),
      new OptimizeCSSAssetsPlugin()
    ]
  },

  /* Plugins */
  plugins: [
    new webpack.ProgressPlugin(),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 8080,
      proxy: 'http://localhost:53200/'
    }),
    /* Public */
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'source/public/'),
          to: '[name][ext]'
        }
      ]
    }),
    /* Paginas */
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/assinatura/assinatura.hbs'),
      filename: 'assinatura.html',
      chunks: ['assinatura']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/assinatura/assinatura-sucesso.hbs'),
      filename: 'assinatura-sucesso.html',
      chunks: ['assinatura-sucesso']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/entrar/entrar.hbs'),
      filename: 'entrar.html',
      chunks: ['entrar']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/erro/erro.hbs'),
      filename: 'erro.html',
      chunks: ['erro']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/telas/telas.hbs'),
      filename: 'telas.html',
      chunks: ['telas']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/telas/telas-visualizar.hbs'),
      filename: 'telas-visualizar.html',
      chunks: ['telas-visualizar']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/painel/painel.hbs'),
      filename: 'painel.html',
      chunks: ['painel']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/perfil/perfil.hbs'),
      filename: 'perfil.html',
      chunks: ['perfil']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/registrar/registrar.hbs'),
      filename: 'registrar.html',
      chunks: ['registrar']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/suporte/suporte.hbs'),
      filename: 'suporte.html',
      chunks: ['suporte']
    }),
    /* CSS */
    new MiniCSSExtractPlugin({
      filename: 'assets/css/[name]-[contenthash:8].css'
    })
  ],

  /* Carregadores */
  module: {
    rules: [
      /* JS */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            cacheDirectory: true
          }
        }
      },
      /* CSS */
      {
        test: /\.css$/,
        use: [
          MiniCSSExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  ['postcss-import', {}],
                  ['postcss-preset-env', {}],
                  ['autoprefixer']
                ]
              }
            }
          }
        ]
      },
      /* HTML */
      {
        test: /\.hbs$/,
        use: [
          {
            loader: 'handlebars-loader',
            options: {
              partialDirs: path.resolve(__dirname, './source/html/partials')
            }
          }
        ]
      }
    ]
  }
}

/* ---- Requires ---- */

const file = require('../helpers/read-file-async')
const path = require('path')

/* ---- Constants ---- */

const PATH_HTML = path.resolve(__dirname, '../../dist')

/* ---- Methods ---- */

exports.renderizar = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/assinatura.html`)
  res.status(200).send(html)
}

exports.renderizarSucesso = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/assinatura-sucesso.html`)
  res.status(200).send(html)
}

/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const ENTRAR_CONTROLLER = require('../controllers/entrar')

/* ---- Methods ---- */

router.get('/', ENTRAR_CONTROLLER.renderizar)
router.get('/entrar', ENTRAR_CONTROLLER.renderizar)

module.exports = router

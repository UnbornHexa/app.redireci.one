/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const SUPORTE_CONTROLLER = require('../controllers/suporte')

/* ---- Methods ---- */

router.get('/', SUPORTE_CONTROLLER.renderizar)

module.exports = router

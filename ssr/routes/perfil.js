/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const PERFIL_CONTROLLER = require('../controllers/perfil')

/* ---- Methods ---- */

router.get('/', PERFIL_CONTROLLER.renderizar)

module.exports = router

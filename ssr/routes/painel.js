/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const PAINEL_CONTROLLER = require('../controllers/painel')

/* ---- Methods ---- */

router.get('/', PAINEL_CONTROLLER.renderizar)

module.exports = router

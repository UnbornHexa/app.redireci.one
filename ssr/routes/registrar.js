/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const REGISTRAR_CONTROLLER = require('../controllers/registrar')

/* ---- Methods ---- */

router.get('/', REGISTRAR_CONTROLLER.renderizar)

module.exports = router

/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const TELAS_CONTROLLER = require('../controllers/telas')

/* ---- Methods ---- */

router.get('/visualizar/:idLink', TELAS_CONTROLLER.renderizarVisualizar)

router.get('/', TELAS_CONTROLLER.renderizar)

module.exports = router

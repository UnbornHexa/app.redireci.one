/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const ASSINATURA_CONTROLLER = require('../controllers/assinatura')

/* ---- Methods ---- */

router.get('/', ASSINATURA_CONTROLLER.renderizar)
router.get('/sucesso', ASSINATURA_CONTROLLER.renderizarSucesso)

module.exports = router

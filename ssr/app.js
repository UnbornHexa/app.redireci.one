/* ---- Requires ---- */

const express = require('express')
const path = require('path')
const morgan = require('morgan')
const serveStatic = require('serve-static')
const compression = require('compression')
const helmet = require('helmet')

const app = express()
const helmetData = require('./data/helmet.json')

/* ---- Middlewares ---- */

app.use(compression())
app.use(morgan('common'))

app.use(helmet.hidePoweredBy())
app.use(helmet.contentSecurityPolicy(helmetData.contentSecurityPolicy))

app.use('/', serveStatic(path.resolve(__dirname, '../dist')))

/* ---- Routes ---- */

// Index

const ENTRAR_ROUTE = require('./routes/entrar')
app.use('/', ENTRAR_ROUTE)

// Pages

const ASSINATURA_ROUTE = require('./routes/assinatura')
const TELAS_ROUTE = require('./routes/telas')
const PAINEL_ROUTE = require('./routes/painel')
const PERFIL_ROUTE = require('./routes/perfil')
const REGISTRAR_ROUTE = require('./routes/registrar')
const SUPORTE_ROUTE = require('./routes/suporte')
app.use('/assinatura', ASSINATURA_ROUTE)
app.use('/telas', TELAS_ROUTE)
app.use('/painel', PAINEL_ROUTE)
app.use('/perfil', PERFIL_ROUTE)
app.use('/registrar', REGISTRAR_ROUTE)
app.use('/suporte', SUPORTE_ROUTE)

// Erro

const ERRO_ROUTE = require('./routes/erro')
app.use('/', ERRO_ROUTE)

module.exports = app

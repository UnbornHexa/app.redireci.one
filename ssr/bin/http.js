/* ---- Requires ---- */

const app = require('../app')
const http = require('http')

/* ---- Constants ---- */

const PORT = '53200'

/* Server */

app.set('port', PORT)
const server = http.createServer(app)

server.listen(PORT)
server.on('error', onError)
server.on('listening', onListening)

console.log('# Servidor Iniciado!')
console.log('# Escutando na porta ' + PORT + '...')

/* Events */

function onError (error) {
  if (error.syscall !== 'listen') throw error

  const bind = (typeof port === 'string')
    ? 'Pipe ' + PORT
    : 'Port ' + PORT

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
    default:
      throw error
  }
}

function onListening () {
  const addr = server.address()
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  console.log('Listening on ' + bind)
}

/* App Error Handler */

process.on('unhandledRejection', (error) => {
  console.log(error)
})
